<div class="modal-cta">
  <div class="box-cta">
    <div class="title">
      <h2>Nós te ligamos</h2>
    </div>
    <div class="cta-form">
      <!-- <form>
        <input type="text" placeholder="Seu Nome">
        <input type="text" placeholder="Seu E-mail">
        <input type="text" placeholder="Seu Telefone">
        <button type="submit"></button>
      </form> -->
      <?php echo do_shortcode('[contact-form-7 id="14" title="Ligamos"]'); ?>
      <div class="close-modal">
        <p>x</p>
      </div>
        <div class="logos">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/bonacert.png" alt="Certificado Craftsman" title="Certificado Craftsman">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/selo-casa-cor-2017.png" alt="Selo Casa Cor 2018" title="Selo Casa Cor 2017">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/selo-casa-cor-2018.png" alt="Selo Casa Cor 2018" title="Selo Casa Cor 2018">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/selo-casa-cor-2019.png" alt="Selo Casa Cor 2019" title="Selo Casa Cor 2019">
        </div>
    </div>
  </div>
</div>