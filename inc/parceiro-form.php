<?php if ( is_page('contato' ) ) { ?>
<div class="form-step" style="padding-top: 165px; padding-bottom: 0; margin-bottom: -4px;">
  <?php
}
else 
{
 ?>
 <div class="form-step" style="padding-top: 165px; padding-bottom: 0">
  <?php } ?>
  <div class="gridD">
    <div class="left"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/man-left.png" alt="Seja Parceiro" title="Seja Parceiro"></div>
    <div class="right">
      <div class="title">
        <h2>Seja parceiro</h2>
        <p>Some conhecimentos e venha fazer parte de nossa história de sucesso. Entre em contato conosco.</p>
      </div>
      <div class="form">
        <?php echo do_shortcode('[contact-form-7 id="4" title="Seja parceiro"]'); ?>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>