<?php if( is_front_page() ) : ?>
<style type="text/css">
	.brown-box {
		background-color: rgba(66, 67, 67, .95);
		position: fixed;
		z-index: 99999;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		display: none;
	}

	.call-now-brown {
		position: absolute;
		background-color: #272727;
		display: block;
	}

	.brown-title {
		color: #f4eacb;
		margin-bottom: 25px;
		font-family: 'Anton', sans-serif;
	}

	.call-now-brown .tagline {
		font-size: 18px;
		color: #FFF;
		line-height: 25px;
		margin-bottom: 33px;
	}

	.green-box {
		width: 100%;
		display: flex;
		align-items: center;
		justify-content: center;
		font-size: 24px;
		color: #fff;
		min-height: 68px;
		background-color: #60c927;
	}

	.green-box span {
		font-size: 40px;
		font-family: "Anton", sans-serif;
		position: relative;
		top: -4px;
		left: 10px;
	}

	@media screen and (max-width: 1000px) {
		.call-now-brown {
			padding: 10px;
			width: 100%;
			top: 0;
			max-width: 320px;
			height: 100vh;
			padding-top: 50px;
		}

		.call-now-brown .tagline {
			line-height: 18px;
			font-size: 12px;
			max-width: 265px;
		}

		.brown-title {
			font-size: 18px;
		}

		.green-box {
			max-width: 240px;
			font-size: 16px;
		}

		.green-box span {
			font-size: 18px;
			top: -1px;
			left: 5px;
		}
	}

	@media screen and (min-width: 1000px) {
		.call-now-brown {
			left: 0;
			right: 0;
			padding: 40px;
			width: 100%;
			max-width: 580px;
			margin: auto;
			left: 0;
			right: 0;
			top: 50%;
			transform: translateY(-50%);
		}

		.brown-title {
			font-size: 35px;
		}
	}
</style>
<div class="brown-box">
	<a class="call-now-brown" href="tel:1147502944" target="_BLANK">
		<p class="brown-title">
			LIGUE E FAÇA SEU ORÇAMENTO AGORA!
		</p>
		<p class="tagline">
			Entre em contato, passamos o orçamento por telefone de maneira rápida e fácil! Fale com nossos especialistas
			e conheça nossas condições especiais. <strong> Vamos negociar?</strong>
		</p>
		<p class="green-box active-trigger">
			LIGUE AGORA:
			<span class="active-trigger">11 3093.2010</span>
		</p>
	</a>
</div>
<script>
	window.onload = function () {
		document.addEventListener('click', function (event) {
			if (!event.target.classList.contains('active-trigger')) {
				$('.brown-box').fadeOut();
			}
		});

		$(document).ready(function (e) {

			function setCookie(name, value, days) {
				var expires = "";
				if (days) {
					var date = new Date();
					date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
					expires = "; expires=" + date.toUTCString();
				}
				document.cookie = name + "=" + (value || "") + expires + "; path=/";
			}

			function getCookie(name) {
				var nameEQ = name + "=";
				var ca = document.cookie.split(';');
				for (var i = 0; i < ca.length; i++) {
					var c = ca[i];
					while (c.charAt(0) == ' ') c = c.substring(1, c.length);
					if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
				}
				return null;
			}

			if (!getCookie('modal')) {
				setCookie('modal', 'Usuário visualizou o modal de certificação nos últimos 7 dias', 1);
				window.setTimeout(function () {
					$('.brown-box').fadeIn();
				}, 5000)
			}
		});
	}
</script>
<?php endif; ?>