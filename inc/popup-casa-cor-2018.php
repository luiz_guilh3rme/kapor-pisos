<style type="text/css">
    .popup-casa-cor-2018{
    	position: fixed;
	    top: 0;
	    right: 0;
	    width: 100%;
	    z-index: 999;
	    background-color: rgba(0,0,0,.8);
	  	opacity: 0;
	    visibility: hidden;
	    transition: all ease 0.2s;
	    height: 100vh;
	}
  	.popup-casa-cor-2018 .box-orcamento{
  		position: relative;
  		display: block;
  		max-width: 805px;
  		top: -50%;
	 	height: 90vh !important;
  		margin: auto;
  		background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/background-pop-up.png);
  		background-repeat: no-repeat;
  		background-size: cover;
  		background-position: center;
    	overflow: hidden !important;
	    transform: translateY(-25%);
	}

	.popup-casa-cor-2018 .box-orcamento picture img {
		position: absolute;
		display: block;
		margin: auto;
		top: 0;
		bottom: 0;
		left: 0;
		right: 0;
		max-width: 100%;
		cursor: pointer;
	}

	.close-modal { 
		position: absolute;
		top: 50px;
		right: 50px;
		width: 30px;
		height: 30px;
		text-align: center;
		cursor: pointer; 
		@media(min-width: 750px) {
			top: 40px;
    		right: 25px;
		}
	}

	.fechar {
		position: absolute;
		top: 13px;
		right: 29px;
	}
	.fechar span {
		position: absolute;
		background-color: #050505;
		width: 29px;
		height: 2px;
	}

	.fechar span:first-child{
		transform: rotate(-137deg) translateX(0px)
	}

	.fechar span:last-child {
		transform: rotate(-47deg) translateX(0px)
	}
</style>
<?php if( is_front_page() ) : ?>
<div class="popup-casa-cor-2019">
  <a href="https://www.kaporpisos.com.br/parceiros" class="box-orcamento">
    <picture>
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-casa-cor-2019.png" />
    </picture>
    <div class="close-modal">
        <p class="fechar">
        	<span>&nbsp;</span>
        	<span>&nbsp;</span>
        </p>
    </div>

  </a>
</div>

<?php endif; ?>