<div class="newsletter-step">
  <div class="gridD">
    <div class="overlay-beige"></div>
    <div class="left">
      <div class="title">
        <h2>CADASTRE E RECEBA NOVIDADES</h2>
        <p> Quer saber mais sobre nossos serviços e produtos em madeira? Deixe seu contato e mantenha-se informado.
</p>
      </div>
      <div class="form-news">
        <!-- <form>
          <input type="text" placeholder="Seu E-mail">
          <button type="submit"></button>
        </form> -->
        <?php echo do_shortcode('[contact-form-7 id="11" title="Newsletter"]'); ?>
      </div>
    </div>
    <div class="right">
      <div class="title">
        <h2>Tire sua dúvida</h2>
        <p> Diga-nos suas dúvidas e,<br>em breve, responderemos :)</p>
      </div>
      <div class="faq-form">
        <!-- <form>
          <input type="text" placeholder="Seu Nome">
          <input type="text" placeholder="Seu E-mail">
          <textarea placeholder="Sua Dúvida"></textarea>
          <button type="submit"></button>
        </form> -->
        <?php echo do_shortcode('[contact-form-7 id="12" title="Dúvidas"]'); ?>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>