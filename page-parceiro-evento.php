<?php
/**
 * Template Name: Modelo Parceiro Evento
 *
 * 
 *
 * @package WordPress
 * @subpackage Kapor_Pisos
 * @since Kapor Pisos 1.0
 */
get_header();

?>
<div class="parceiros-step">
	<div class="gridD">
		<div class="left">
			<div class="topic-header">
				<ul>
              <!-- <li>Assoalhos de madeira</li>
              	<li>Assoalhos de demolição</li> -->
              </ul>
          </div>
          <div class="content-resume-categoria">
          	<div class="title">
          		<h1><?php the_title(); ?></h1>
          	</div>
          	<div class="content">
          		<?php if (is_page('casa-cor-2016')): ?>
      				<!-- sem imagem -->
          		<?php endif ?>	          		
          		<?php if (is_page('casa-cor-2017')): ?>
          			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/casa-cor-2017.png" alt="Parceria Casa Cor" title="Nosso Parceiro Casacor" style="max-width: 100%">
          		<?php endif ?>	
          		<?php if (is_page('casa-cor-2018')): ?>
          			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/selo-casa-cor-2018.png" alt="Parceria Casa Cor" title="Nosso Parceiro Casacor" style="max-width: 100%">
          		<?php endif ?>	


          		<!--  <p> Com matéria-prima nobre e um portfólio rico e atraente, procuramos por parceiros que venham para somar e tornar nossos produtos e serviços ainda melhores e mais reconhecidos. Venha ser parceiro da Kapor Pisos em Madeira. Junte-se a nós.</p> -->
<!--               <div class="more-details"><a href="#form-step" title="">Seja um Parceiro</a></div>
-->            </div>
</div>
</div>
<div class="right">
	<div class="banner-categoria-produtos">

		<?php if (is_page('casa-cor-2016')): ?>
		<div class="slide">
			<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/casa-cor-2016-img-1.jpg);"></div>
		</div>
		<div class="slide">
			<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/casa-cor-2016-img-2.jpg);"></div>
		</div>
		<div class="slide">
			<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/casa-cor-2016-img-3.jpg);"></div>
		</div>
		<div class="slide">
			<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/casa-cor-2016-img-4.jpg);"></div>
		</div>
		<div class="slide">
			<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/casa-cor-2016-img-5.jpg);"></div>
		</div>
		<div class="slide">
			<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/casa-cor-2016-img-6.jpg);"></div>
		</div>
		<?php endif; ?>

		<?php if (is_page('casa-cor-2017')): ?>
		<div class="slide">
			<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/casa-cor-2017-img-1.jpg);"></div>
		</div>
		<div class="slide">
			<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/casa-cor-2017-img-2.jpg);"></div>
		</div>
		<div class="slide">
			<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/casa-cor-2017-img-3.jpg);"></div>
		</div>
		<div class="slide">
			<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/casa-cor-2017-img-4.jpg);"></div>
		</div>
		<div class="slide">
			<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/casa-cor-2017-img-5.jpg);"></div>
		</div>
		<div class="slide">
			<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/casa-cor-2017-img-6.jpg);"></div>
		</div>
		<?php endif; ?>

		<?php if (is_page('casa-cor-2018')): ?>
		<div class="slide">
			<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/casa-cor-2018-img-1.jpg);"></div>
		</div>
		<div class="slide">
			<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/casa-cor-2018-img-2.jpg);"></div>
		</div>
		<div class="slide">
			<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/casa-cor-2018-img-3.jpg);"></div>
		</div>
		<div class="slide">
			<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/casa-cor-2018-img-4.jpg);"></div>
		</div>
		<div class="slide">
			<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/casa-cor-2018-img-5.jpg);"></div>
		</div>
		<div class="slide">
			<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/casa-cor-2018-img-6.jpg);"></div>
		</div>
		<?php endif; ?>
	</div>
</div>
</div>
<div class="clearfix"></div>
</div>
<div class="second-step-parceiros">
	<div class="gridD">
		<div class="text-center">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<p>
					<?php the_content(); ?>
				</p>
			<?php endwhile; endif; ?>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<?php get_template_part( 'inc/parceiro-form' ); ?>
<?php get_template_part( 'inc/cadastre-form' ); ?>
<?php get_footer();?>