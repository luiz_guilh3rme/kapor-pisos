<?php
/**
 * 
 *
 * 
 *
 * @package WordPress
 * @subpackage Kapor_Pisos
 * @since Kapor Pisos 1.0
 */
get_header();
?>
<style type="text/css">
.aplication-step .left{
  margin-top: 0;
}
</style>
<div class="servicos-step">
  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="gridD">
      <div class="left">
        <div class="content-resume-servicos">
          <div class="title">
            <h1><?php the_title(); ?></h1>
          </div>
          <div class="content">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/bona_cert.png" alt="Certificado Craftsman" title="Certificado Craftsman">
            <div class="more-details"><a href="#" title="Faça um Orçamento">faça um orçamento</a></div>
          </div>
        </div>
      </div>
      <div class="right">
        <div class="title">
          <h2>INFORMAÇÕES ADICIONAIS</h2>
        </div>
        <div class="content">
          <?php the_content(); ?>
        </div>
        <button class="mobile-cta-budget more-details">
          faça um orçamento
        </button>
      </div>
    </div>
  <?php endwhile; endif; ?>
  <div class="clearfix"></div>
</div>
<div class="details-step-servicos">
  <div class="gridD">
    <div class="title-products">
      <div class="title">
        <h2>Outras aplicações</h2>
      </div>
    </div>
    <div class="products">
      <?php $related_args = array(
        'post_type' => 'servicos',
        'posts_per_page' => 5,
        'post__not_in' => array( get_the_ID() ),
      );
      $related = new WP_Query( $related_args );
      if( $related->have_posts() ) :
        ?>
        <?php while( $related->have_posts() ): $related->the_post(); ?>
          <div class="product"><?php the_post_thumbnail('servicos-thumb'); ?>
            <div class="title-static">
              <h2><?php the_title(); ?></h2>
            </div>
            <div class="overlay-product">
              <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><h2><?php the_title(); ?></h2></a>
              <?php //the_excerpt(); ?>
              <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-right-white.png" alt="Ira para <?php the_title(); ?>" title="Ira para <?php the_title(); ?>"></a>
            </div>
          </div>
        <?php endwhile; ?>
      <?php endif; ?>
    </div>
  </div>
  <div class="clearfix"></div>
</div>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <!--<div class="destaq-photo">
      <div class="gridD"><?php the_post_thumbnail(); ?></div>
      <div class="clearfix"></div>
    </div>-->
    
    <div class="aplication-step">
      <div class="gridD">
        <div class="left">
          <div class="title">
            <h2>Serviços de <?php the_title(); ?></h2>
          </div>
        <?php endwhile; endif; ?>
        <div class="content">
          <p><?php echo rwmb_meta( 'aplica_1' ); ?></p>
          <p><?php echo rwmb_meta( 'aplica_2' ); ?></p>
        </div>
          <!-- <div class="metrics">
            <ul>
              <li>Tamanho</li>
              <li>7x2cm</li>
              <li>10x2cm</li>
              <li>15x2cm</li>
              <li>20x2cm</li>
            </ul>
          </div> -->
          <!-- <div class="metrics">
            <ul>
              <li>VEJA MAIS OPÇÕES DE ASSOALHOS</li><a href="#" title="">ver mais<img src="<?php //echo get_template_directory_uri(); ?>/assets/img/arrow-right-blue.png" alt="" title=""></a>
            </ul>
          </div> -->
        </div>
        
        <div class="right">
          <div class="title">
            <h2>Faça seu orçamento agora</h2>
          </div>
          <div class="orcamento-form">
            <!-- <form>
              <input type="text" placeholder="Seu Nome">
              <input type="text" placeholder="Seu E-mail">
              <input type="text" placeholder="Seu Telefone">
              <div class="two-colums-form">
                <select>
                  <option value="--">Motivo</option>
                  <option value="--">Motivo2</option>
                </select>
                <select>
                  <option value="--">Tipos de madeira</option>
                  <option value="--">Tipos de madeira2      </option>
                </select>
              </div>
              <div class="two-colums-form">
                <select>
                  <option value="--">Marcas</option>
                  <option value="--">Marcas2</option>
                </select>
                <select>
                  <option value="--">Quantidade</option>
                  <option value="--">Quantidade2  </option>
                </select>
              </div>
              <textarea placeholder="Sua Mensagem"></textarea>
              <button type="submit"></button>
            </form> -->
            <?php echo do_shortcode('[contact-form-7 id="13" title="Orçamento"]'); ?>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
    <!-- <div class="form-step">
      <div class="gridD">
        <div class="left"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/man-left.jpg" alt="" title=""></div>
        <div class="right">
          <div class="title">
            <h2>Seja parceiro</h2>
            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
          </div>
          <div class="form">
            <form>
              <input type="text" placeholder="Seu Nome">
              <input type="text" placeholder="Seu E-mail">
              <input type="text" placeholder="Seu Telefone">
              <textarea placeholder="Sua Mensagem"></textarea>
              <button type="submit"></button>
            </form>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div> -->
    <?php get_template_part( 'inc/parceiro-form' ); ?>
    <?php get_template_part( 'inc/cadastre-form' ); ?>
    <!-- <div class="newsletter-step">
      <div class="gridD">
        <div class="overlay-beige"></div>
        <div class="left">
          <div class="title">
            <h2>CADASTRE E RECEBA NOVIDADES</h2>
            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
          </div>
          <div class="form-news">
            <form>
              <input type="text" placeholder="Seu E-mail">
              <button type="submit"></button>
            </form>
          </div>
        </div>
        <div class="right">
          <div class="title">
            <h2>Tire sua dúvida</h2>
            <p> Diga-nos suas dúvidas e <br>em breve responderemos :)</p>
          </div>
          <div class="faq-form">
            <form>
              <input type="text" placeholder="Seu Nome">
              <input type="text" placeholder="Seu E-mail">
              <textarea placeholder="Sua Dúvida"></textarea>
              <button type="submit"></button>
            </form>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div> -->
    <?php get_footer();?>