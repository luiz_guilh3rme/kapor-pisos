<?php
/**
 * Template 404
 *
 * 
 *
 * @package WordPress
 * @subpackage Kapor_Pisos
 * @since Kapor Pisos 1.0
 */
get_header();
?>

    <div class="about-us-home">
      <div class="gridD">
        <div class="left">
          <div class="title">
          <br>
          <br>
          <br>
          <br>
          <br>
            <h1>Ops, Erro 404!</h1>
            <br>
            <br>
          </div>
          <div class="content">
            <h2>A página solicitada <br>não pode ser encontrada.</h2>
            <br>
            <a href="http://www.kaporpisos.com.br" title="Ir para Home">Retorna para Home
            <br>
            <br></a>

          </div>
         
        </div>
        <div class="right"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/about-us-photo.jpg" alt="" title="" height="705" width="1095"></div>
      </div>
      <div class="clearfix"></div>
    </div>

    <?php get_footer();?>

