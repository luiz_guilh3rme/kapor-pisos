<?php
/**
 * Template Kapor Pisos
 *
 * 
 *
 * @package WordPress
 * @subpackage Kapor_Pisos
 * @since Kapor Pisos 1.0
 */
?>
<!-- FOOTER-->
<footer>
	<div class="gridD">
		<div class="navs">
			<div class="nav">
				<h2>Institucional</h2>
				<ul>
					<li><a href="http://www.kaporpisos.com.br/empresa/" title="Ir para Quem Somos">A Empresa</a></li>
					<li><a href="http://www.kaporpisos.com.br/parceiros/" title="Ir para Parceiros">Parceiros</a></li>
					<li><a href="http://www.kaporpisos.com.br/galeria/" title="Ir para Galeira">Galeira</a></li>
					<li><a href="http://www.kaporpisos.com.br/blog/" title="Ir para Blog">Blog</a></li>
					<li><a href="http://www.kaporpisos.com.br/contato/" title="Ir para Contato">Contato</a></li>
				</ul>
			</div>
			<div class="nav">
				<h2>Serviços</h2>
				<ul>
					<li><a href="http://www.kaporpisos.com.br/aplicacao-de-resina-bona/" title="Ir para Aplicação de Resina Bona">Aplicação de Resina Bona</a>
					</li>
					<li><a href="http://www.kaporpisos.com.br/aplicacao-de-resina-bona/" title ="Ir para Aplicação de Resina Bona ">Aplicação de Resina Bona</a>
					</li>
					<li><a href="http://www.kaporpisos.com.br/aplicacao-de-resina-sinteco/" title ="Ir para Aplicação de Resina Sinteco ">Aplicação de Resina Sinteco</a></li>
					<li><a href="http://www.kaporpisos.com.br/aplicacao-de-resina-skania/" title ="Ir para Aplicação de Resina Skania ">Aplicação de Resina Skania</a></li>
					<li><a href="http://www.kaporpisos.com.br/instalacao-e-acabamento-de-piso/" title ="Ir para Instalação e Acabamento de Piso ">Instalação e Acabamento de Piso</a></li>
					<li><a href="http://www.kaporpisos.com.br/raspagem-de-taco-e-aplicacao-de-resina/" title ="Ir para Raspagem de Taco e Aplicação de Resina ">Raspagem de Taco e Aplicação de Resina</a></li>
					<li><a href="http://www.kaporpisos.com.br/raspagem-e-aplicacao-de-resina-bona/" title ="Ir para Raspagem e Aplicação de Resina Bona ">Raspagem e Aplicação de Resina Bona</a></li>

				</ul>
			</div>
			<div class="nav">
				<h2>Produtos</h2>
				<ul>

					<li><a href="http://www.kaporpisos.com.br/assoalho-de-demolicao/" title ="Ir para Assoalho de Demolição ">Assoalho de Demolição</a></li>
					<li><a href="http://www.kaporpisos.com.br/assoalhos-de-madeira-tradicionais/" title ="Ir para Assoalho de Madeira ">Assoalho de Madeira</a></li>
					<li><a href="http://www.kaporpisos.com.br/brise-de-madeira/" title ="Ir para Brisé de Madeira ">Brisé de Madeira</a></li>
					<li><a href="https://www.kaporpisos.com.br/carpete-de-nylon/" title ="Ir para Carpetes de Nylon">Carpetes de Nylon</a></li>
					<li><a href="http://www.kaporpisos.com.br/corrimao-de-madeira/" title ="Ir para Corrimão de Madeira ">Corrimão de Madeira</a></li>
					<li><a href="http://www.kaporpisos.com.br/deck-de-madeira/" title ="Ir para Deck de Madeira ">Deck de Madeira</a></li>
					<li><a href="http://www.kaporpisos.com.br/escadas-de-madeira/" title ="Ir para Escadas de Madeira ">Escadas de Madeira</a></li>
					<li><a href="http://www.kaporpisos.com.br/forro-de-madeira/" title ="Ir para Forro de Madeira ">Forro de Madeira</a></li>
					<li><a href="http://www.kaporpisos.com.br/painel-de-madeira/" title ="Ir para Painel de Madeira ">Painel de Madeira</a></li>
					<li><a href="https://www.kaporpisos.com.br/pergolados-de-madeira" title ="Ir para Pergolado de Madeira">Pergolado de Madeira</a></li>
					<li><a href="http://www.kaporpisos.com.br/piso-de-bambu/" title ="Ir para Piso de Bambu ">Piso de Bambu</a></li>
					<li><a href="http://www.kaporpisos.com.br/piso-de-madeira/" title ="Ir para Piso de Madeira ">Piso de Madeira</a></li>
					<li><a href="http://www.kaporpisos.com.br/piso-laminado/" title ="Ir para Piso Laminado ">Piso Laminado</a></li>
					<li><a href="http://www.kaporpisos.com.br/piso-pronto-estruturado/" title ="Ir para Piso Pronto Estruturado ">Piso Pronto Estruturado</a></li>
					<li><a href="http://www.kaporpisos.com.br/piso-vinilico/" title ="Ir para Piso Vinílico ">Piso Vinílico</a></li>
					<li><a href="https://www.kaporpisos.com.br/rodapes-de-madeira" title ="Ir para Rodapé de Madeira">Rodapé de Madeira</a></li>
					<li><a href="http://www.kaporpisos.com.br/vigamento-para-telhado/" title ="Ir para Vigamento para Telhados ">Vigamento para Telhados</a></li>
				</ul>
			</div>
			<div class="nav">
				<div class="badges">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/bonacert.png" alt="Certificado Craftsman" title="Certificado Craftsman">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/selo-casa-cor-2017.png" alt="Selo Casa Cor 2018" title="Selo Casa Cor 2017">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/selo-casa-cor-2018.png" alt="Selo Casa Cor 2018" title="Selo Casa Cor 2018">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/selo-casa-cor-2019.png" alt="Selo Casa Cor 2019" title="Selo Casa Cor 2019">
				</div>
			</div>
		</div>
		<div class="adress-footer">
			<div class="logo-footer"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-footer.png" alt="Logo Kapor Pisos" title="Logo Kapor Pisos" height="51" width="164"></div>
			<div class="adress">
				<p><span>Showroom</span><br>Rua Alvarenga, nº 1104 – Butantã – São Paulo/SP<br>Tel: <a href="tel:1147502944" itemprop="telephone"><span>(11) 4750-2944</span></a><br>Horário de funcionamento (Showroom)<br>Seg a sex das 8 às 19:00 hrs<br>Aos sábados das 8 às 15:00 hrs</p>
			</div>
			<div class="adress">
				<p><span>Deposito:</span><br>Av. Corifeu de Azevedo Marques, nº 700 –<br>Butantã – São Paulo/SP<br>Tel: <a href="tel:1137232970" itemprop="telephone"><span>(11) 3723-2970</span></a><br>Horário de funcionamento<br>Seg a sex das 8 às 19:00 hrs</p>
			</div>
			<div class="adress">
				<p><span>Serraria/Deposito:</span><br>Email de vendas: vendas@kapor.com.br: contato@kapor.com.br</p>
			</div>
		</div>
	</div>
	<div class="social-mobile">
		<h2>Siga-nos
			<ul>
				<li><a href="https://www.facebook.com/pisodemadeirakapor" title="Ir para Facebook" rel="nofollow"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<li><a href="#" title=""><i class="fa fa-twitter" aria-hidden="true" rel="nofollow"></i></a></li>
				<li><a href="#" title=""><i class="fa fa-instagram" aria-hidden="true" rel="nofollow"></i></a></li>
				<li><a href="#" title=""><i class="fa fa-youtube-play" aria-hidden="true" rel="nofollow"></i></a></li>
			</ul>
		</h2>

	</div>
	<div class="clearfix"></div>
</footer>
<?php get_template_part( 'inc/popup-ligacao' ); ?>
<div class="modal-orcamento">
	<div class="box-orcamento">
		<div class="orcamento-form">
			<?php echo do_shortcode('[contact-form-7 id="13" title="Orçamento"]'); ?>
			<div class="close-modal">
				<p>x</p>
			</div>
		</div>
	</div>
</div>
<?php get_template_part( 'inc/ligamos-form' ); ?>
<div class="cta-mobile">
	<div class="left">
		<h2>Nós te ligamos</h2>
	</div>
<!-- 	<div class="whatsapp-mobile visible-xs right whatsapp" id="whatsapp" >    
		<h2><a href="" class="link-whats whats-mobile" title="Fale conosco no Whatsapp!" target="_blank"><i class="fa fa-whatsapp"></i> Envie
		um Whatsapp</a></h2>
	</div> -->
</div>
<div class="modal-cta-mobile">
	<form>
		<input type="text" placeholder="Seu Nome">
		<input type="text" placeholder="Seu Telefone">
		<button type="submit"></button>
	</form>
</div>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/lib/jquery-1.11.1.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/lib/modernizr-custom.js"></script>
<!-- INCLUDE JS-->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/lib/slick.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/script.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/leads_.js"></script>
<style type="text/css" media="screen">
	html body .modal-orcamento .box-orcamento .orcamento-form form input[type=submit] {
		display: block;
		width: 100%;
		height: 45px;
		padding: 10px;
		border: 2px solid #fff;
		color: #FFF !important;
		background-color: transparent;
		font-family: raleway-regular, sans-serif;
		font-size: 18px;
		margin-bottom: 15px;
		background: none;
		position: static;
		text-indent: 0 !important;
		transition: all .25s ease;
	}

	html body .modal-orcamento .box-orcamento .orcamento-form form input[type=submit]:hover {
		background-color: #594a37;
	}

	@media screen and (max-width: 1000px) {
		.depo-sec .img-left {
			display: none !important;
		}

		.formtitle:before {
			content: none !important;
		}

		.depo-sec .desc-right {
			width: 100% !important;
		}

		.oque-fazemos .left-mobile .thumb-oque {
			min-height: 235px !important; 
		}

		.mobile-cta-budget {
			display: block !important;
			font-weight: 200;
			color: #f4eacb;
			font-size: 20px;
			text-transform: uppercase;
			background-color: #829194;
			padding: 10px;
			margin: 0 auto 20px auto;
			border: 0;
		}

		html body .popup-casa-cor-2018 .box-orcamento,
		html body .modal-orcamento .box-orcamento, 
		html body .modal-orcamento-sem-cookie .box-orcamento {
			height: 90vh;
			top: 50%;
			left: 0;
			right: 0;
			margin: auto;
			transform: translateY(-25%);
			width: 97.5%;
			overflow-y: scroll;
		}

		html body .modal-orcamento-sem-cookie .box-orcamento .orcamento-form form {
			padding: 0 0px 0 10px;
		}

		html body .servicos-step .left .content-resume-servicos .title {
			padding: 0 0 20px 0;
		}

		html body .right #map {
			margin-top: 130px;
		}

		html body .cta-form-contact-wrapper {
			margin: 50px 0;
		}

		html body .cta-form-contact-wrapper form {
			width: 95%;
			margin: auto;
		}
	}

	body .cta-form-contact-wrapper {
		margin-top: 50px;
		padding: 50px 10px 25px 10px;
		background-color: #829194;
	}

	body .cta-form-contact-wrapper form {
		width: 100%;
		max-width: 525px;
		margin: auto;
	}

	body .cta-form-contact-wrapper .formtitle {
		font-family: anton,sans-serif;
		text-transform: uppercase;
		font-size: 48px;
		color: #f4eacb;
		font-weight: 400;
		margin-bottom: 20px;
		text-align: center;
		position: relative;
	}

	.formtitle:before {
		content: " ";
		position: absolute;
		top: 38px;
		left: 0;
		display: block;
		height: 4px;
		width: 40px;
		background-color: #594a37;
	}

	body .cta-form-contact-wrapper input, body .cta-form-contact-wrapper textarea {
		display: block;
		width: 100%;
		height: 45px;
		padding: 15px;
		border: 2px solid #fff;
		color: #FFF !important;
		background-color: transparent;
		font-family: raleway-regular, sans-serif;
		font-size: 18px;
		margin-bottom: 15px;
	}

	body .cta-form-contact-wrapper input[type="submit"] {
		transition: all .15s ease;
		padding: 0;
		cursor: pointer;
	}

	body .cta-form-contact-wrapper input[type="submit"]:hover {
		background-color: #594a37;
	}

	body .cta-form-contact-wrapper input::placeholder, body .cta-form-contact-wrapper textarea::placeholder {
		color: #FFF;
	}

	body .ondeestamos-step .left .content-resume-categoria .content .more-details a {
		font-size: 17px;
	}

	body .cta-form-contact-wrapper textarea {
		height: 200px;
	}

	body .cta-form-contact-wrapper p {
		display: block;
		width: 100%;
	}


	.form-step {
		clear: both !important;
	}

	.mobile-cta-budget {
		display: none;
	}
	.modal-orcamento{
			 /* visibility: visible;
			 opacity: 1;*/
			}
			.modal-orcamento .box-orcamento{
				top: 600px;
			}
			.screen-reader-response{
				display: none;
			}
			.wpcf7-not-valid-tip{
				margin-top: -11px;
				margin-bottom: 12px;
				color: red;
				float: left;
				width: 100%;
			}
			.wpcf7-response-output{
				color: red;
			}
			.details-step .right .orcamento-form form button, .details-step .right .orcamento-form form input[type="submit"] {
				height: 30px;
				width: 35px;
				text-indent: -99999px;
				background-position: -202px 2px;
				border: 0;
				cursor: pointer;
				background-color: transparent;
				position: relative;
				right: 22px;
				float: right;
				bottom: inherit!important;
				top: -52px;
			}
			.form-step .right .form form button, .form-step .right .form form input[type="submit"] {
				height: 30px;
				width: 35px;
				text-indent: -99999px;
				background-position: -162px 2px;
				border: 0;
				cursor: pointer;
				background-color: transparent;
				position: relative;
				right: 30px;
				bottom: 0;
				float: right;
				top: -66px;
			}
			.form-step{
				height: auto;
				padding-bottom: 75px;
			}
			.newsletter-step .left .title {
				padding-right: 20%;
			}
			.newsletter-step .left .form-news form button, .newsletter-step .left .form-news form input[type="submit"] {
				height: 30px;
				width: 35px;
				text-indent: -99999px;
				background-position: -162px 2px;
				border: 0;
				cursor: pointer;
				background-color: transparent;
				position: relative;
				right: 20px;
				float: right;
				top: -50px;
			}
			.newsletter-step .right .faq-form form button, .newsletter-step .right .faq-form form input[type=submit] {
				height: 30px;
				width: 35px;
				text-indent: -99999px;
				background-position: -202px 2px;
				border: 0;
				cursor: pointer;
				background-color: transparent;
				position: relative;
				right: 19px;
				bottom: 53px;
				float: right;
			}
			.wpcf7-response-output {
				color: red;
				float: left;
				width: 100%;
			}
			.form-news .wpcf7-not-valid-tip{
				display: none;

			}

			.form-news .wpcf7-response-output{
				margin-top: -45px;
			}
			.box-cta .wpcf7-not-valid-tip{
				display: none;
			}
			.ver{
				visibility: visible;
				opacity: 1;
				transition: all ease 0.2s;
			}

			.blog-post.--parceiros .post {
				background: #829194 !important;
			}

			@media screen and (max-width: 480px) {
				.blog-post.--parceiros {
					display: block !important;
					margin-right: auto !important;
					padding: 0 15px !important;
				}

				.blog-post.--parceiros .gridD { 
					padding-left: 15px !important;
					padding-right: 15px !important;
					width: 100% !important;
				}

				.blog-post.--parceiros .post {
					float: none !important;
					display: block !important;
					margin-right: auto !important;
					width: 100% !important;
					margin-bottom: 40px !important;
				}

				.second-step-parceiros {
					padding: 0 15px;
					margin: auto;
				}

				.ngg-gallery-thumbnail-box { 
					float: none; 
				}

				.ngg-gallery-thumbnail {
					margin: 5px auto;
					width: 240px;
				}

				.modal-orcamento .box-orcamento .orcamento-form form button {
					bottom: -445px;
				}
			}

			.whatsapp {
				background-color: #01E675;
			}

			#whatsapp{
				background-color: #01E675;
			}
			
			#whatsapp h2{
				font-size: 20px;
			}    
		</style>
		<script type="text/javascript">
			$('.todos-depoimentos').slick({
				autoplay: true,
				autoplaySpeed: 4000,
				dots: false,
				arrows: false,
				fade: true,
				cssEase: 'linear'
			});
		</script>
		<script type="text/javascript">
			$(document).ready(function(e) {
				$(".more-details a, .mobile-cta-budget, .btn-wood a").click(function(){
					$('.modal-orcamento-sem-cookie').addClass('ver')
					$('.modal-orcamento-sem-cookie .box-orcamento').animate({ 'top' : '50%'})
				})
				$(".modal-orcamento-sem-cookie .close-modal").click(function(){
					$('.modal-orcamento-sem-cookie').removeClass('ver')
					$('.modal-orcamento-sem-cookie .box-orcamento').animate({ 'top' : '-50%'})
				})
			})
		</script>



		<script type="application/ld+json">{
			"@context": "http://schema.org",
			"@type": "LocalBusiness",
			"name": "Kapor Pisos em Madeira",
			"image": "https://www.kaporpisos.com.br/wp-content/themes/kapor/assets/img/logo.png",
			"url": "http://www.kaporpisos.com.br/",
			"telephone": [ "11 3723-2970" ],
			"address": {
			"@type": "PostalAddress",
			"streetAddress": " Rua Alvarenga, nº 1104 – Butantã – São Paulo/SP",
			"addressLocality": "São Paulo",
			"postalCode": "05509-002",
			"addressCountry": "Brazil",
			"addressRegion": "São Paulo"
		},
		"location": {
		"@type": "Place",
		"geo": {
		"@type": "GeoCircle",
		"geoRadius": "0"
	}
},
"openingHours": [
"Mo-Fr 08:00-18:00"
],
"priceRange": "9999,99"
}</script> 


<script type="application/ld+json">{
	"@context": "http://schema.org",
	"@type": "Organization",
	"name": "Kapor Pisos em Madeira",
	"url": "http://www.kaporpisos.com.br/",
	"logo": "https://www.kaporpisos.com.br/wp-content/themes/kapor/assets/img/logo.png"
}</script> 


<script type="application/ld+json">{
	"@context": "http://schema.org",
	"@type": "Person",
	"name": "Kapor Pisos em Madeira",
	"url": "http://www.kaporpisos.com.br/",
	"homeLocation": {
	"@type": "Place",
	"address": {
	"@type": "PostalAddress",
	"addressCountry": "Brazil"
}
}
}</script> 


<script type="application/ld+json">{
	"@context": "http://schema.org",
	"@type": "WebSite",
	"name": "Kapor Pisos em Madeira",
	"alternateName": "Kapor Pisos em Madeira",
	"url": "http://www.kaporpisos.com.br/"
}</script> 

<script type="application/ld+json">
	{
		"@context": "http://schema.org",
		"@type": "BreadcrumbList",
		"itemListElement":
		[
		{
			"@type": "ListItem",
			"position": 1,
			"item":
			{
				"@id": "https://www.kaporpisos.com.br",
				"name": "Dresses"
			}
		}
		]
	}
</script>

<script type="application/ld+json">
	{
		"@context" : "http://schema.org",
		"@type" : "Organization",
		"name" : "Kapor Pisos em Madeira",
		"url" : "https://www.kaporpisos.com.br/index.php",
		"sameAs" : [
		"https://www.facebook.com/pisodemadeirakapor",
		"https://www.instagram.com/kaporpisos/"
		]
	}
</script>

</body>
</html>