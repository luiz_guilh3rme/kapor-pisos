<?php
/**
 * Template Name: Modelo Blog
 *
 * 
 *
 * @package WordPress
 * @subpackage Kapor_Pisos
 * @since Kapor Pisos 1.0
 */
get_header();
?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="empresa-step">
      <div class="gridD">
        <div class="left">
          <div class="topic-header">
            <ul>
              <!-- <li>Assoalhos de madeira</li>
              <li>Assoalhos de demolição</li> -->
            </ul>
          </div>
          <div class="content-resume-categoria">
            <div class="title">
              <h2>Blog</h2>
            </div>
            <div class="content">
              <p>Quer ficar por dentro das principais novidades do ramo de produtos em madeira? É só acompanhar nosso blog! Aqui você encontra dicas e se mantém informado sobre tendências de uso da madeira em arquitetura e decoração.</p>
              <!-- <div class="more-details"><a href="#" title="">faça um orçamento</a></div> -->
            </div>
          </div>
        </div>
        <div class="right">
          <div class="banner-categoria-produtos">
            <div class="slide">
              <div class="img"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="second-step-empresa">
      <div class="gridD">
        <div style="width: 100%" class="left">
          <div class="title">
            <h2><?php the_title(); ?></h2>
          </div>
          <div class="content">
            <?php the_content(); ?>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
<?php endwhile; endif; ?>
    <!-- <div class="form-step">
      <div class="gridD">
        <div class="left"><img src="<?php //echo get_template_directory_uri(); ?>/<?php echo get_template_directory_uri(); ?>/assets/img/man-left.jpg" alt="" title=""></div>
        <div class="right">
          <div class="title">
            <h2>Seja parceiro</h2>
            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
          </div>
          <div class="form">
            <form>
              <input type="text" placeholder="Seu Nome">
              <input type="text" placeholder="Seu E-mail">
              <input type="text" placeholder="Seu Telefone">
              <textarea placeholder="Sua Mensagem"></textarea>
              <button type="submit"></button>
            </form>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div> -->
    <?php get_template_part( 'inc/parceiro-form' ); ?>
    <?php get_template_part( 'inc/cadastre-form' ); ?>
    <div style="margin-bottom: -35px;" class="seja-parceiro"><a href="#" alt=""><img src="<?php echo get_template_directory_uri(); ?>/<?php echo get_template_directory_uri(); ?>/assets/img/seja-parceiro-mobile.jpg" alt="" title=""></a></div>
    <!-- <div class="newsletter-step">
      <div class="gridD">
        <div class="overlay-beige"></div>
        <div class="left">
          <div class="title">
            <h2>CADASTRE E RECEBA NOVIDADES</h2>
            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
          </div>
          <div class="form-news">
            <form>
              <input type="text" placeholder="Seu E-mail">
              <button type="submit"></button>
            </form>
          </div>
        </div>
        <div class="right">
          <div class="title">
            <h2>Tire sua dúvida</h2>
            <p> Diga-nos suas dúvidas e <br>em breve responderemos :)</p>
          </div>
          <div class="faq-form">
            <form>
              <input type="text" placeholder="Seu Nome">
              <input type="text" placeholder="Seu E-mail">
              <textarea placeholder="Sua Dúvida"></textarea>
              <button type="submit"></button>
            </form>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div> -->
    <?php get_footer();?>