<?php
/**
* Template Name: Modelo Blog
*
* @package WordPress
* @subpackage Kapor_Pisos
* @since Kapor Pisos 1.0
*/

get_header();
?>
<div class="blog-step">
  <div class="gridD">
    <div class="left">
      <div class="content-resume-categoria">
        <div class="title">
          <h1>Blog</h1>
        </div>
        <div class="content">
          <p>Quer ficar por dentro das principais novidades do ramo de produtos em madeira? É só acompanhar nosso blog! Aqui você encontra dicas e se mantém informado sobre tendências de uso da madeira em arquitetura e decoração.</p>
        </div>
      </div>
    </div>
    <div class="right">
      <div class="banner-categoria-produtos">
        <div class="slide">
          <div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/slide-blog1.jpg);"></div>
        </div>
        <div class="slide">
          <div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/slide-blog2.jpg);"></div>
        </div>
        <div class="slide">
          <div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/slide-blog3.jpg);"></div>
        </div>
        <div class="slide">
          <div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/slide-blog4.jpg);"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>
<div class="second-step-blog">
  <div class="gridD">
    <div class="left fuck-this">
      <div class="title">
        <h2>Últimas postagens</h2>
      </div>
      <?php 
      $args = array( 'post_type' => 'post', 'posts_per_page' => -1 );
      $loop = new WP_Query( $args );
      while ( $loop->have_posts() ) : $loop->the_post(); ?>
        <div class="post"><?php the_post_thumbnail('blog-full'); ?>
        <div class="title-static">
          <h2><?php the_title(); ?></h2>
        </div>
        <div class="overlay-product">
          <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> 
           <h2><?php the_title(); ?></h2>
           <?php the_excerpt(); ?>
           <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-right-white.png" alt="Ira para <?php the_title(); ?>" title="Ira para <?php the_title(); ?>">
         </a>
       </div>
     </div>
   <?php endwhile; ?>
 </div>
</div>
<div class="clearfix"></div>
</div>

<!-- <div class="form-step">
<div class="gridD">
<div class="left"><img src="<?php //echo get_template_directory_uri(); ?>/<?php echo get_template_directory_uri(); ?>/assets/img/man-left.jpg" alt="" title=""></div>
<div class="right">
<div class="title">
<h2>Seja parceiro</h2>
<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
</div>
<div class="form">
<form>
<input type="text" placeholder="Seu Nome">
<input type="text" placeholder="Seu E-mail">
<input type="text" placeholder="Seu Telefone">
<textarea placeholder="Sua Mensagem"></textarea>
<button type="submit"></button>
</form>
</div>
</div>
</div>
<div class="clearfix"></div>
</div> -->
<?php get_template_part( 'inc/parceiro-form' ); ?>
<?php get_template_part( 'inc/cadastre-form' ); ?>
<div style="margin-bottom: -35px;" class="seja-parceiro"><a href="#" alt=""><img src="<?php echo get_template_directory_uri(); ?>/<?php echo get_template_directory_uri(); ?>/assets/img/seja-parceiro-mobile.jpg" alt="" title=""></a></div>
<!-- <div class="newsletter-step">
<div class="gridD">
<div class="overlay-beige"></div>
<div class="left">
<div class="title">
<h2>CADASTRE E RECEBA NOVIDADES</h2>
<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
</div>
<div class="form-news">
<form>
<input type="text" placeholder="Seu E-mail">
<button type="submit"></button>
</form>
</div>
</div>
<div class="right">
<div class="title">
<h2>Tire sua dúvida</h2>
<p> Diga-nos suas dúvidas e <br>em breve responderemos :)</p>
</div>
<div class="faq-form">
<form>
<input type="text" placeholder="Seu Nome">
<input type="text" placeholder="Seu E-mail">
<textarea placeholder="Sua Dúvida"></textarea>
<button type="submit"></button>
</form>
</div>
</div>
</div>
<div class="clearfix"></div>
</div> -->
<?php get_footer();?>