<?php
/**
 * Template Name: Modelo Parceiros Sem link Eventos
 *
 * 
 *
 * @package WordPress
 * @subpackage Kapor_Pisos
 * @since Kapor Pisos 1.0
 */
get_header();

?>
<div class="parceiros-step">
	<div class="gridD">
		<div class="left">
			<div class="topic-header">
				<ul>
              <!-- <li>Assoalhos de madeira</li>
              	<li>Assoalhos de demolição</li> -->
              </ul>
          </div>
          <div class="content-resume-categoria">
          	<div class="title">
          		<h1><?php the_title(); ?></h1>
          	</div>
          	<div class="content">
          		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/selo-casa-cor2.png" alt="Parceria Casa Cor" title="Nosso Parceiro Casacor" style="max-width: 100%">
          		<!--  <p> Com matéria-prima nobre e um portfólio rico e atraente, procuramos por parceiros que venham para somar e tornar nossos produtos e serviços ainda melhores e mais reconhecidos. Venha ser parceiro da Kapor Pisos em Madeira. Junte-se a nós.</p> -->
<!--               <div class="more-details"><a href="#form-step" title="">Seja um Parceiro</a></div>
-->            </div>
</div>
</div>
<div class="right">
	<div class="banner-categoria-produtos">

		<div class="slide">
			<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/parceiros02.jpg);"></div>
		</div>
		<div class="slide">
			<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/parceiros03.jpg);"></div>
		</div>
		<div class="slide">
			<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/parceiros04.jpg);"></div>
		</div>
		<div class="slide">
			<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/parceiros05.jpg);"></div>
		</div>
		<div class="slide">
			<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/parceiros06.jpg);"></div>
		</div>

	</div>
</div>
</div>
<div class="clearfix"></div>
</div>
<div class="second-step-parceiros">
	<div class="gridD">
		<div class="text-center">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<h2><?php the_title(); ?></h2>
				<p>
					<?php the_content(); ?>
				</p>
			<?php endwhile; endif; ?>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<?php get_template_part( 'inc/parceiro-form' ); ?>
<?php get_template_part( 'inc/cadastre-form' ); ?>
<?php get_footer();?>