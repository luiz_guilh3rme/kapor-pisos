<?php
/**
 * 
 *
 * 
 *
 * @package WordPress
 * @subpackage Kapor_Pisos
 * @since Kapor Pisos 1.0
 */
get_header();
?>
<style type="text/css">
.aplication-step .left{
  margin-top: 0;
}
</style>
<div class="categoria-step">
  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="gridD">
      <div class="left">
        <div class="topic-header">
          <ul>
            <li>Produtos</li>
            <li><?php echo get_the_title(); ?></li>
          </ul>
        </div>
        <div class="content-resume-categoria">
          <div class="title">
            <h1><?php the_title(); ?></h1>
          </div>
          <div class="content">
            <?php the_excerpt(); ?>
            <div class="more-details"><a href="#" title="Faça um orçamento">faça um orçamento</a></div>
          </div>
        </div>
      </div>
      <div class="right">
        <div class="banner-categoria-produtos">
          <div class="slide">
            <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'produtos-destaque' );?>
            <div class="img" style="background-image: url('<?php echo $thumb['0'];?>')"></div>
          </div>
        </div>
        <button class="mobile-cta-budget more-details">
          faça um orçamento
        </button>
      </div>
    </div>
    <div class="clearfix"></div>
  <?php endwhile; endif; ?>
</div>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<div class="wood-types">
  <div class="gridD">
      <div class="title">
        <h2>Tipos de madeira</h2>
      </div>
      <?php if(get_field('woods')): ?>
          <div class="row-flex list-woods">
              <?php $i = 0; while(has_sub_field('woods')): ?>
                <div class="col-xs-15 col-sm-15 item-wood">
                    <img src="<?php the_sub_field('image_wood'); ?>" title="<?php the_sub_field('title_wood'); ?>" alt="<?php the_sub_field('title_wood'); ?>">
                    <h2><?php the_sub_field('title_wood'); ?></h2>
                  <div class="btn-wood">
                    <a href="#" id="dtitle" class="btn btn-wood2" data-toggle="modal" data-target="#myModal" data-title="<?php the_sub_field('title_wood'); ?>" data-tpage="<?php the_title()?>">Faça um orçamento</a>
                  </div>
                </div>
                <?php if($i % 5 == 0): ?>
                <div class="clearfix"></div>
                <?php endif;$i++; ?>
              <?php endwhile; ?>
          </div>
      <?php endif; ?>
  </div>
</div> 
<script>		
$('a.btn.btn-wood2').on('click', function(e) {  // Ao clique do botão
    var tmadeira = $(this).attr('data-title');  
    var tpage = $(this).attr('data-tpage');  // Define o valor da variável title como o valor do data-title
    $('input[name="tipodemadeira"]').attr("placeholder", tmadeira);
	$('input[name="piso"]').attr("placeholder", tpage);
	// Define valor do placeholder do input que tem atributo name com valor de tipomadeira.
});


</script>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
        <div class="modal-body">
			<?php echo do_shortcode('[contact-form-7 id="2032" title="Orcamento piso"]'); ?>
        </div>
      </div>
      
    </div>
  </div>
<div class="modal-orcamento">
	<div class="box-orcamento">
		<div class="orcamento-form">
			<?php echo do_shortcode('[contact-form-7 id="13" title="Orçamento"]'); ?>
			<div class="close-modal">
				<p>x</p>
			</div>
		</div>
	</div>
</div>
<div class="second-step-categorias">
  <div class="gridD">
    <div class="left">
      <div class="title">
        <h2>Outros Produtos</h2>
      </div>
      <?php $related_args = array(
        'post_type' => 'produtos',
        'posts_per_page' => 2,
        'post__not_in' => array( get_the_ID() ),
      );
      $related = new WP_Query( $related_args );
      if( $related->have_posts() ) :
        ?>
        <?php while( $related->have_posts() ): $related->the_post(); ?>
          <div class="post"><?php the_post_thumbnail('produtos-thumb'); ?>
            <div class="title-static">
              <h2><?php the_title(); ?></h2>
            </div>
            <div class="overlay-product">
              <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><h2><?php the_title(); ?></h2></a>
              <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_excerpt(); ?></a>
              <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-right-white.png" alt="Ira para <?php the_title(); ?>" title="Ira para <?php the_title(); ?>"></a>
            </div>
          </div>
        <?php endwhile; ?>
      <?php endif; ?>
<!--       <div class="post"><img src="<?php //echo get_template_directory_uri(); ?>/assets/img/blog-2.jpg" alt="" title="">
        <div class="title-static">
          <h2>Deck de madeira</h2>
        </div>
        <div class="overlay-product">
          <h2>Deck de madeira</h2>
          <p>O assoalho tradicional é um produto altamente resistente e durável, deixando o revestimento do seu piso, com um ar mais nobre e sofisticado.</p><a href="#" title=""><img src="<?php //echo get_template_directory_uri(); ?>/assets/img/arrow-right-white.png" alt="" title=""></a>
        </div>
      </div> -->
    </div>
    <div class="right">
      <div class="title">
        <h2>CONFIRA OS BENEFÍCIOS</h2>
      </div>
      <?php while ( have_posts() ) : the_post(); ?>
        <div class="content">
          <?php the_content(); ?>
        </div>
      <?php endwhile; ?>
    </div>
  </div>
  <div class="clearfix"></div>
</div>
<div class="details-step">
  <div class="gridD">
    <div class="left">
      <?php $related_args = array(
        'post_type' => 'produtos',
        'posts_per_page' => 2,
        'post__not_in' => array( get_the_ID() ),
        'offset' => 3,
      );
      $related = new WP_Query( $related_args );
      if( $related->have_posts() ) :
        ?>
        <?php while( $related->have_posts() ): $related->the_post(); ?>
          <div class="post"><?php the_post_thumbnail('produtos-thumb'); ?>
            <div class="title-static">
              <h2><?php the_title(); ?></h2>
            </div>
            <div class="overlay-product">
              <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><h2><?php the_title(); ?></h2></a>
              <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_excerpt(); ?></a>
              <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-right-white.png" alt="Ira para <?php the_title(); ?>" title="Ira para <?php the_title(); ?>"></a>
            </div>
          </div>
        <?php endwhile; ?>
      <?php endif; ?>
    </div>
    <div class="right">
      <div class="title">
        <h2>Faça seu orçamento agora</h2>
      </div>
      <div class="orcamento-form">
        <!-- <form>
          <input type="text" placeholder="Seu Nome">
          <input type="text" placeholder="Seu E-mail">
          <input type="text" placeholder="Seu Telefone">
          <div class="two-colums-form">
            <select>
              <option value="--">Motivo</option>
              <option value="--">Motivo2</option>
            </select>
            <select>
              <option value="--">Tipos de madeira</option>
              <option value="--">Tipos de madeira2      </option>
            </select>
          </div>
          <div class="two-colums-form">
            <select>
              <option value="--">Marcas</option>
              <option value="--">Marcas2</option>
            </select>
            <select>
              <option value="--">Quantidade</option>
              <option value="--">Quantidade2  </option>
            </select>
          </div>
          <textarea placeholder="Sua Mensagem"></textarea>
          <button type="submit"></button>
        </form> -->
        <?php echo do_shortcode('[contact-form-7 id="13" title="Orçamento"]'); ?>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>
<!-- <div class="form-step">
      <div class="gridD">
        <div class="left"><img src="<?php //echo get_template_directory_uri(); ?>/assets/img/man-left.jpg" alt="" title=""></div>
        <div class="right">
          <div class="title">
            <h2>Seja parceiro</h2>
            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
          </div>
          <div class="form">
            <form>
              <input type="text" placeholder="Seu Nome">
              <input type="text" placeholder="Seu E-mail">
              <input type="text" placeholder="Seu Telefone">
              <textarea placeholder="Sua Mensagem"></textarea>
              <button type="submit"></button>
            </form>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div> -->
    <?php get_template_part( 'inc/parceiro-form' ); ?>
    <?php get_template_part( 'inc/cadastre-form' ); ?>
    <!-- <div class="newsletter-step">
      <div class="gridD">
        <div class="overlay-beige"></div>
        <div class="left">
          <div class="title">
            <h2>CADASTRE E RECEBA NOVIDADES</h2>
            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
          </div>
          <div class="form-news">
            <form>
              <input type="text" placeholder="Seu E-mail">
              <button type="submit"></button>
            </form>
          </div>
        </div>
        <div class="right">
          <div class="title">
            <h2>Tire sua dúvida</h2>
            <p> Diga-nos suas dúvidas e <br>em breve responderemos :)</p>
          </div>
          <div class="faq-form">
            <form>
              <input type="text" placeholder="Seu Nome">
              <input type="text" placeholder="Seu E-mail">
              <textarea placeholder="Sua Dúvida"></textarea>
              <button type="submit"></button>
            </form>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div> -->
    <?php get_footer();?>