<?php
/**
 * Template Name: Home
 *
 * 
 *
 * @package WordPress
 * @subpackage Kapor_Pisos
 * @since Kapor Pisos 1.0
 */
get_header();
?>
<style type="text/css">
  .depo-sec .desc-right .content p {
    font-size: 14px;

}
</style>
<div class="banner-full">
      <div class="slide">
        <div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/slide1.jpg);"></div>
      </div>
      <div class="slide">
        <div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/slide2.jpg);"></div>
      </div>
      <div class="slide">
        <div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/slide3.jpg);"></div>
      </div>
      <div class="slide">
        <div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/slide4.jpg);"></div>
      </div>
      <div class="slide">
        <div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/slide5.jpg);"></div>
      </div>
      <div class="slide">
        <div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/slide6.jpg);"></div>
      </div>
      <div class="slide">
        <div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/slide7.jpg);"></div>
      </div>
      
    </div>
    <div class="about-us-home">
      <div class="gridD">
        <div class="left">
          <div class="title">
            <h1>Sobre a Kapor Pisos</h1>
          </div>
          <div class="content">
            <p>Desde sua fundação, em 1945, a Kapor tem trabalhado, crescido e se tornado referência no ramo de revestimentos em madeira. Em décadas de tradição, a Kapor já realizou o sonho de muitas pessoas de obter pisos e demais produtos em madeira de alta qualidade, auxiliando na composição de ambientes bonitos, modernos e confortáveis. Nossos produtos e serviços são executados por experientes e capacitados profissionais. Responsabilidade, eficiência e profissionalismo são os valores que norteiam nosso trabalho.</p><a href="/empresa" title="Ir para Sobre Nós">mais</a>
          </div>
        </div>
        <div class="right"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/institucional-home.jpg" alt="Institucional Home" title="Institucional Home" height="705" width="1095"></div>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="oque-fazemos">
      <div class="gridD">
        <div class="left">
          <div class="thumb-oque"><a href="http://www.kaporpisos.com.br/raspagem-de-taco-e-aplicacao-de-resina/" title="Ir para Raspagem de Taco"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/raspagem-de-tacos-thumb.png" alt="Raspagem de Taco" title="Raspagem de Taco" height="auto" width="227">
              <h2>Raspagem de Taco</h2></a></div>

          <div class="thumb-oque"><a href="http://www.kaporpisos.com.br/raspagem-e-aplicacao-de-resina-bona/" title="Ir para Raspagem e aplicação de resina Bona"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/Raspagem-e-Aplicacao-de-Resina-Bona-thumb.jpg" alt="Raspagem e aplicação de resina Bona" title="Raspagem e aplicação de resina Bona" height="auto" width="227">
              <h2>Raspagem e aplicação de resina Bona</h2></a></div>

          <div class="thumb-oque"><a href="http://www.kaporpisos.com.br/instalacao-e-acabamento-de-piso/" title="Ir para Instalação e Acabamento de Piso"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/Instalacao-e-Acabamento-de-Piso-thumb.png" alt="Instalação e Acabamento de Piso" title="Instalação e Acabamento de Piso" height="auto" width="227">
              <h2>INSTALAÇÃO E ACABAMENTO</h2></a></div>

          <div class="thumb-oque"><a href="http://www.kaporpisos.com.br/aplicacao-de-resina-bona/" title="Ir para Aplicação de Resinas Bona"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/aplicacao-de-resina-bona-thumb.png" alt="Aplicação de Resinas Bona" title="Aplicação de Resinas Bona" height="auto" width="227">
              <h2>APLICAÇÃO DE BONA</h2></a></div>

          <div class="thumb-oque"><a href="http://www.kaporpisos.com.br/aplicacao-de-resina-sinteco/" title="Ir para Aplicação de Sinteco"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/aplicacao-de-resina-sinteco-thumb.png" alt="Aplicação de Sinteco" title="Aplicação de Sinteco" height="auto" width="227">
              <h2>APLICAÇÃO DE SINTECO</h2></a></div>

          <div class="thumb-oque"><a href="http://www.kaporpisos.com.br/aplicacao-de-resina-skania/" title="Ir para Aplicação de Resina Skania"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/aplicacao-de-resina-skania-thumb.png" alt="Aplicação de Resina Skania" title="Aplicação de Resina Skania" height="auto" width="227">
              <h2>APLICAÇÃO DE RESINA SKANIA</h2></a></div>

          <div class="more-details"><a href="#" title="Fazer um Orçamento">faça um orçamento</a></div>
        </div>
        <div class="right">
          <div class="title">
            <h2>O QUE FAZEMOS?</h2>
          </div>
          <div class="content">
            <p>Trabalhamos com os mais diversos tipos de produtos em madeira. Além de diferenciados tipos de piso (assoalho, tacos, pisos laminados, pisos vinílicos e pisos estruturados), também oferecemos brisés, painéis, pergolados, decks, escadas, forros, rodapés, vigamentos e revestimentos diversos. Em cada projeto, utilizamos exclusivamente as mais nobres espécies de madeira, caracterizadas por sua resistência e pela atratividade estética incomparável, levando a beleza rústica da madeira aos mais diversos ambientes. Além disso, nossos processos são marcados pela sustentabilidade, com respeito ao meio ambiente. Nossa madeireira oferece materiais certificados e emprega as tecnologias mais avançadas em rigorosas linhas de produção.
            <p>
            Oferecemos também os serviços de aplicação de resinas (os melhores produtos do mercado), raspagem, instalação e acabamentos de tacos e demais pisos. Contamos com equipes de profissionais experientes e qualificados, executando cada projeto com agilidade, praticidade e dedicação. Estamos prontos para orientar nossos clientes em cada projeto e realizar sonhos.</p><!-- <a href="#" title=""><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-right.png" alt="" title="" height="14" width="59"></a> -->
          </div>
          <div class="overlay-blue"></div>
        </div>
        <div class="left-mobile">
          <div class="title">
            <h2>O QUE FAZEMOS?</h2>
          </div>
           <div class="thumb-oque"><a href="http://www.kaporpisos.com.br/raspagem-de-taco-e-aplicacao-de-resina/" title="Ir para Raspagem de Taco"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/raspagem-de-tacos-thumb.png" alt="Raspagem de Taco" title="Raspagem de Taco" height="auto" width="227">
              <h2>Raspagem de Taco</h2></a></div>

          <div class="thumb-oque"><a href="http://www.kaporpisos.com.br/raspagem-e-aplicacao-de-resina-bona/" title="Ir para Raspagem e aplicação de resina Bona"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/Raspagem-e-Aplicacao-de-Resina-Bona-thumb.jpg" alt="Raspagem e aplicação de resina Bona" title="Raspagem e aplicação de resina Bona" height="auto" width="227">
              <h2>Raspagem e aplicação de resina Bona</h2></a></div>

          <div class="thumb-oque"><a href="http://www.kaporpisos.com.br/instalacao-e-acabamento-de-piso/" title="Ir para Instalação e Acabamento de Piso"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/Instalacao-e-Acabamento-de-Piso-thumb.png" alt="Instalação e Acabamento de Piso" title="Instalação e Acabamento de Piso" height="auto" width="227">
              <h2>INSTALAÇÃO E ACABAMENTO</h2></a></div>

          <div class="thumb-oque"><a href="http://www.kaporpisos.com.br/aplicacao-de-resina-bona/" title="Ir para Aplicação de Resinas Bona"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/aplicacao-de-resina-bona-thumb.png" alt="Aplicação de Resinas Bona" title="Aplicação de Resinas Bona" height="auto" width="227">
              <h2>APLICAÇÃO DE BONA</h2></a></div>

          <div class="thumb-oque"><a href="http://www.kaporpisos.com.br/aplicacao-de-resina-sinteco/" title="Ir para Aplicação de Sinteco"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/aplicacao-de-resina-sinteco-thumb.png" alt="Aplicação de Sinteco" title="Aplicação de Sinteco" height="auto" width="227">
              <h2>APLICAÇÃO DE SINTECO</h2></a></div>

          <div class="thumb-oque"><a href="http://www.kaporpisos.com.br/aplicacao-de-resina-skania/" title="Ir para Aplicação de Resina Skania"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/aplicacao-de-resina-skania-thumb.png" alt="Aplicação de Resina Skania" title="Aplicação de Resina Skania" height="auto" width="227">
              <h2>APLICAÇÃO DE RESINA SKANIA</h2></a></div>
          <div class="more-details"><a href="#" title="Fazer um Orçamento">faça um orçamento</a></div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="clients">
      <div class="gridD">
        <div class="title">
          <h2>Clientes</h2>
        </div>
        <div class="border-top"></div>
        <div class="slide-clients">
          <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client.jpg" alt="Universidade de São Paulo" title="Universidade de São Paulo" height="96" width="175"></div>
          <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-2.jpg" alt="Senai" title="Senai" height="96" width="175"></div>
          <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-3.jpg" alt="Itaú" title="Itaú" height="96" width="175"></div>
          <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-4.jpg" alt="Rede Globo" title="Rede Globo" height="96" width="175"></div>
          <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-5.jpg" alt="Rede bandeirantes" title="Rede bandeirantes" height="96" width="175"></div>
          <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-6.jpg" alt="OAS" title="OAS" height="96" width="175"></div>
          <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-7.jpg" alt="Banco Votorantim" title="Banco Votorantim" height="96" width="175"></div>
        </div>
        <div class="border-bottom"></div>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="clients-mobile">
      <div class="gridD">
        <div class="title">
          <h2>Clientes</h2>
        </div>
        <div class="border-top"></div>
        <div class="slide-clients-mobile">
          <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client.jpg" alt="Universidade de São Paulo" title="Universidade de São Paulo" height="96" width="175"></div>
          <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-2.jpg" alt="Senai" title="Senai" height="96" width="175"></div>
          <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-3.jpg" alt="Itaú" title="Itaú" height="96" width="175"></div>
          <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-4.jpg" alt="Rede Globo" title="Rede Globo" height="96" width="175"></div>
          <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-5.jpg" alt="Rede bandeirantes" title="Rede bandeirantes" height="96" width="175"></div>
          <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-6.jpg" alt="OAS" title="OAS" height="96" width="175"></div>
          <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-7.jpg" alt="Banco Votorantim" title="Banco Votorantim" height="96" width="175"></div>
        </div>
        <div class="border-bottom"></div>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="products-home">
      <div class="gridD">
        <div class="title-products">
          <div class="title">
            <h2>Produtos em destaque</h2>
            <p>Confira alguns de nossos principais produtos. A durabilidade e a beleza única da madeira levam sofisticação e elegância aos ambientes.</p>
          </div>
          <div class="bt-more"><a href="#" title="Fazer um Orçamento">faça um orçamento</a></div>
        </div>
        <div class="products">
          <div class="product"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-home-1.png" alt="" title="">
            <div class="title-static">
              <h2>Assoalho Tradicionais</h2>
            </div>
            <div class="overlay-product"><a href="https://www.kaporpisos.com.br/assoalhos-de-madeira-tradicionais/" title="Ir para Assoalho Tradicionais">
                <h2>Assoalho Tradicionais</h2></a><a href="https://www.kaporpisos.com.br/assoalhos-de-madeira-tradicionais/" title="Ir para Assoalho Tradicionais">
                <p>O assoalho tradicional é um produto altamente resistente e durável, deixando o revestimento do seu piso, com um ar mais nobre e sofisticado.</p></a><a href="https://www.kaporpisos.com.br/assoalhos-de-madeira-tradicionais/" title="Ir para Assoalho Tradicionais"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-right-white.png" alt="Assoalho Tradicionais" title="Assoalho Tradicionais"></a></div>
          </div>
          <div class="product"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-home-2.png" alt="" title="">
            <div class="title-static">
              <h2>Deck de madeira</h2>
            </div>
            <div class="overlay-product"><a href="http://www.kaporpisos.com.br/deck-de-madeira/" title="Ir para Deck de madeira">
                <h2>Deck de madeira</h2></a><a href="http://www.kaporpisos.com.br/deck-de-madeira/" title="Ir para Deck de madeira">
                <p>O assoalho tradicional é um produto altamente resistente e durável, deixando o revestimento do seu piso, com um ar mais nobre e sofisticado.</p></a><a href="http://www.kaporpisos.com.br/deck-de-madeira/" title="Ir para Deck de madeira"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-right-white.png" alt="Deck de madeira" title="Deck de madeira"></a></div>
          </div>
          <div class="product"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-home-3.jpg" alt="" title="">
            <div class="title-static">
              <h2>piso pronto</h2>
            </div>
            <div class="overlay-product"><a href="http://www.kaporpisos.com.br/piso-pronto-estruturado/" title="Ir para Piso Pronto">
                <h2>Piso Pronto</h2></a><a href="http://www.kaporpisos.com.br/piso-pronto-estruturado/" title="Ir para Piso Pronto">
                <p>O assoalho tradicional é um produto altamente resistente e durável, deixando o revestimento do seu piso, com um ar mais nobre e sofisticado.</p></a><a href="http://www.kaporpisos.com.br/piso-pronto-estruturado/" title="Ir para Piso Pronto"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-right-white.png" alt="Piso Pronto" title="Piso Pronto"></a></div>
          </div>
          <div class="product"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-home-4.png" alt="" title="">
            <div class="title-static">
              <h2>Piso Vinílico</h2>
            </div>
            <div class="overlay-product"><a href="http://www.kaporpisos.com.br/piso-vinilico" title="Ir para Piso Vinílico">
                <h2>Piso Vinílico</h2></a><a href="http://www.kaporpisos.com.br/piso-vinilico" title="Ir para Piso Vinílico">
                <p>Os pisos vinílicos são produzidos com um revestimento de PVC. São geralmente leves e de espessura baixa, porém, com alta durabilidade.</p></a><a href="http://www.kaporpisos.com.br/piso-vinilico" title="Ir para Piso Vinílico"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-right-white.png" alt="Piso Vinílico" title="Piso Vinílico"></a>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="products-home-mobile">
      <div class="gridD">
        <div class="title-products">
          <div class="title">
            <h2>Produtos em destaque</h2>
            <p>Confira alguns de nossos principais produtos. A durabilidade e a beleza única da madeira levam sofisticação e elegância aos ambientes.</p>
          </div>
          <div class="bt-more"><a href="#" title="">faça um orçamento</a></div>
        </div>
        <div class="products-mobile">
          <div class="product-mobile"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-home-1.png" alt="" title="">
            <div class="title-static">
              <h2>Assoalho Tradicionais</h2>
            </div>
            <div class="overlay-product"><a href="http://www.kaporpisos.com.br/assoalho-de-demolicao/" title="">
                <h2>Assoalho Tradicionais</h2></a><a href="http://www.kaporpisos.com.br/assoalho-de-demolicao/" title="">
                <p>O assoalho tradicional é um produto altamente resistente e durável, deixando o revestimento do seu piso, com um ar mais nobre e sofisticado.</p></a><a href="http://www.kaporpisos.com.br/assoalho-de-demolicao/" title="Ir para Assoalho Tradicionais"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-right-white.png" alt="Assoalho Tradicionais" title="Assoalho Tradicionais"></a></div>
          </div>
          <div class="product-mobile"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-home-2.png" alt="" title="">
            <div class="title-static">
              <h2>Deck de madeira</h2>
            </div>
            <div class="overlay-product"><a href="http://www.kaporpisos.com.br/deck-de-madeira/" title="Ir para Deck de madeira">
                <h2>Deck de madeira</h2></a><a href="http://www.kaporpisos.com.br/deck-de-madeira/" title="Ir para Deck de madeira">
                <p>O assoalho tradicional é um produto altamente resistente e durável, deixando o revestimento do seu piso, com um ar mais nobre e sofisticado.</p></a><a href="http://www.kaporpisos.com.br/deck-de-madeira/" title="Ir para Deck de madeira"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-right-white.png" alt="Deck de madeira" title="Deck de madeira"></a></div>
          </div>
          <div class="product-mobile"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-home-3.jpg" alt="" title="">
            <div class="title-static">
              <h2>piso pronto</h2>
            </div>
            <div class="overlay-product"><a href="http://www.kaporpisos.com.br/piso-pronto-estruturado/" title="Ir para Piso Pronto">
                <h2>Piso Pronto</h2></a><a href="http://www.kaporpisos.com.br/piso-pronto-estruturado/" title="Ir para Piso Pronto">
                <p>O assoalho tradicional é um produto altamente resistente e durável, deixando o revestimento do seu piso, com um ar mais nobre e sofisticado.</p></a><a href="http://www.kaporpisos.com.br/piso-pronto-estruturado/" title="Ir para Piso Pronto"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-right-white.png" alt="Piso Pronto" title="Piso Pronto"></a></div>
          </div>
          <div class="product-mobile"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-home-4.png" alt="" title="">
            <div class="title-static">
              <h2>Piso Vinílico</h2>
            </div>
            <div class="overlay-product"><a href="http://www.kaporpisos.com.br/piso-vinilico" title="Ir para Piso Vinílico">
                <h2>Piso Vinílico</h2></a><a href="http://www.kaporpisos.com.br/piso-vinilico" title="Ir para Piso Vinílico">
                <p>Os pisos vinílicos são produzidos com um revestimento de PVC. São geralmente leves e de espessura baixa, porém, com alta durabilidade.</p></a><a href="http://www.kaporpisos.com.br/piso-vinilico" title="Ir para Piso Vinílico"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-right-white.png" alt="Piso Vinílico" title="Piso Vinílico"></a>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="todos-depoimentos">
    <?php $args = array( 'post_type' => 'depoimento', 'posts_per_page' => 3 );
    $loop = new WP_Query( $args );
    while ( $loop->have_posts() ) : $loop->the_post(); ?>
   <div class="depo-sec">
      <div class="gridD">
        <div class="img-left">
          <div class="overlay-beige"></div><?php the_post_thumbnail('depo-thumb'); ?>
        </div>
        <div class="desc-right">
          <div class="title">
            <h2>Depoimentos</h2>
          </div>
          <div class="content">
            <?php the_content(); ?>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>



    <?php endwhile; ?>
  </div>
    <!-- <div class="seja-parceiro"><a href="#" alt=""><img src="<?php //echo get_template_directory_uri(); ?>/assets/img/seja-parceiro-mobile.jpg" alt="" title="" height="auto" width="300"></a></div> -->
    <!-- <div class="form-step">
      <div class="gridD">
        <div class="left"><img src="<?php //echo get_template_directory_uri(); ?>/assets/img/man-left.png" alt="" title=""></div>
        <div class="right">
          <div class="title">
            <h2>Seja parceiro</h2>
            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
          </div>
          <div class="form">
            <form>
              <input type="text" placeholder="Seu Nome">
              <input type="text" placeholder="Seu E-mail">
              <input type="text" placeholder="Seu Telefone">
              <textarea placeholder="Sua Mensagem"></textarea>
              <button type="submit"></button>
            </form>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div> -->
    <?php get_template_part( 'inc/parceiro-form' ); ?>
    <?php get_template_part( 'inc/cadastre-form' ); ?>
    <!-- <div class="newsletter-step">
      <div class="gridD">
        <div class="overlay-beige"></div>
        <div class="left">
          <div class="title">
            <h2>CADASTRE E RECEBA NOVIDADES</h2>
            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
          </div>
          <div class="form-news">
            <form>
              <input type="text" placeholder="Seu E-mail">
              <button type="submit"></button>
            </form>
          </div>
        </div>
        <div class="right">
          <div class="title">
            <h2>Tire sua dúvida</h2>
            <p> Diga-nos suas dúvidas e <br>em breve responderemos :)</p>
          </div>
          <div class="faq-form">
            <form>
              <input type="text" placeholder="Seu Nome">
              <input type="text" placeholder="Seu E-mail">
              <textarea placeholder="Sua Dúvida"></textarea>
              <button type="submit"></button>
            </form>
          </div>
        </div>
      </div>
    </div> -->
    <div class="blog-post">
      <div class="gridD">
        <div class="center">
          <div class="title">
            <h2>blog kapor</h2>
            <p>Fique por dentro das principais novidades do ramo de produtos em madeira, além de dicas de manutenção e decoração.</p>
          </div>
        </div>
        <?php $args = array( 'post_type' => 'post', 'posts_per_page' => 2 );
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post(); ?>
        <div class="post">
          <?php the_post_thumbnail('blog-home'); ?>
          <div class="title-static">
            <h2><?php the_title(); ?></h2>
          </div>
          <div class="overlay-product">
              <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><h2><?php the_title(); ?></h2></a>
              <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_excerpt(); ?></a>
              <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-right-white.png" alt="Ir Para <?php the_title(); ?>" title="Ir Para <?php the_title(); ?>"></a>
          </div>
        </div>
        <?php endwhile; ?>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="blog-post-mobile">
      <div class="gridD">
        <div class="center">
          <div class="title">
            <h2>blog kapor</h2>
            <p>Fique por dentro das principais novidades do ramo de produtos em madeira, além de dicas de manutenção e decoração.</p>
          </div>
        </div>
        <div class="post-slide-home">
          <div class="post-mobile"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/blog-1.png" alt="" title="">
            <div class="title-static">
              <h2>Deck de madeira</h2>
            </div>
            <div class="overlay-product">
              <h2>Deck de madeira</h2>
              <p>O assoalho tradicional é um produto altamente resistente e durável, deixando o revestimento do seu piso, com um ar mais nobre e sofisticado.</p><a href="#" title=""><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-right-white.png" alt="" title=""></a>
            </div>
          </div>
          <div class="post-mobile"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/blog-2.png" alt="" title="">
            <div class="title-static">
              <h2>Deck de madeira</h2>
            </div>
            <div class="overlay-product">
              <h2>Deck de madeira</h2>
              <p>O assoalho tradicional é um produto altamente resistente e durável, deixando o revestimento do seu piso, com um ar mais nobre e sofisticado.</p><a href="#" title=""><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-right-white.png" alt="" title=""></a>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
    <?php get_footer();?>