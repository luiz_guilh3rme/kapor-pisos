<?php
/**
 * Template Name: Modelo Empresa
 *
 * 
 *
 * @package WordPress
 * @subpackage Kapor_Pisos
 * @since Kapor Pisos 1.0
 */
get_header();
?>
<div class="empresa-step">
      <div class="gridD">
        <div class="left">
          <div class="topic-header">
            <ul>
              <li>Sobre Nós</li>
              <li>A Empresa</li>
            </ul>
          </div>
          <div class="content-resume-categoria">
            <div class="title">
              <h1>A Kapor</h1>
            </div>
            <div class="content">
              <p>A Kapor Revestimentos de Madeira é uma das empresas mais conceituadas e renomadas no ramo, fundada em 1945. Possuímos um grande know-how em relação ao trabalho com revestimentos em madeiras nobres.</p>
              <div class="more-details"><a href="#" title="Fazer um Orçamento">faça um orçamento</a></div>
            </div>
          </div>
        </div>
        <div class="right">
          <div class="banner-categoria-produtos">
            <div class="slide">
              <div class="img"></div>
            </div>
            <!-- <div class="slide">
              <div class="img"></div>
            </div> -->
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="second-step-empresa">
      <div class="gridD">
        <div class="left">
          <div class="title">
            <h2>COM O QUE TRABALHAMOS</h2>
          </div>
          <div class="content">
            <p>Trabalhamos com diferentes tipos de revestimentos, obtidos a partir de matérias-primas de qualidade, incluindo tacos, decks, lambris, pisos prontos e revestimentos de escada e de parede. Os diferenciais que nos levaram ao destaque no mercado são a alta capacitação de nossos funcionários e a qualidade de nossos produtos. Além disso, levando em consideração a crescente preocupação com noções de sustentabilidade e respeito ao meio ambiente, empregamos matérias-primas provenientes de reflorestamento.
            </p>
            <p>
            A Madeireira Kapor obteve reconhecimento no mercado por conta de seu profissionalismo, responsabilidade e eficácia, tanto nos projetos de grande porte quanto nos menores. Oferecemos produtos diversificados, como: assoalhos, tacos, forros, decks, madeira de demolição, painéis de parede, escadas e pisos prontos em madeira maciça. Todos estes itens possuem certificação de qualidade e garantia de 5 anos. Além disso, oferecemos os melhores preços do mercado.
            </p>
            <p>
            Nosso atendimento é de alta qualidade e confiança, de modo que construímos excelentes relacionamentos com nossos clientes.
            </p>
          </div>
        </div>
        <div class="right">
          <div class="title">
            <h2>Estrutura</h2>
          </div>
          <div class="content">
            <p>Nossas linhas de produção para cada produto são rigorosas. Dispomos de estufas e equipamentos de alta tecnologia, de modo que seja possível entregar nossos produtos a cada cliente de forma rápida e segura.
            <p>
            Para que você saiba mais sobre a excelência de nossos serviços, possuímos um Show Room, com área de 600m², localizado no Bairro do Butantã, em São Paulo, em parceria firmada com a empresa Arquiteto e Construtoras. Venha nos visitar e conheça nossas linhas de produtos. São décadas de dedicação e trabalho, oferecendo aos clientes peças em madeira dignas de palácios.
            <p>
            Entre em contato conosco. Para qualquer produto ou serviço que você deseje, nossa equipe estará pronta para orientá-lo da melhor forma.
            </p>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="clients-empresa">
      <div class="gridD">
        <div class="title">
          <h2>Clientes</h2>
        </div>
        <div class="border-top"></div>
        <div class="slide-clients">
          <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client.jpg" alt="Universidade de São Paulo" title="Universidade de São Paulo" height="96" width="175"></div>
          <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-2.jpg" alt="Senai" title="Senai" height="96" width="175"></div>
          <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-3.jpg" alt="Itaú" title="Itaú" height="96" width="175"></div>
          <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-4.jpg" alt="Rede Globo" title="Rede Globo" height="96" width="175"></div>
          <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-5.jpg" alt="Rede bandeirantes" title="Rede bandeirantes" height="96" width="175"></div>
          <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-6.jpg" alt="OAS" title="OAS" height="96" width="175"></div>
          <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-7.jpg" alt="Banco Votorantim" title="Banco Votorantim" height="96" width="175"></div>
        </div>
        <div class="border-bottom"></div>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="clients-mobile">
      <div class="gridD">
        <div class="title">
          <h2>Clientes</h2>
        </div>
        <div class="border-top"></div>
        <div class="slide-clients-mobile">
          <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client.jpg" alt="Universidade de São Paulo" title="Universidade de São Paulo" height="96" width="175"></div>
          <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-2.jpg" alt="Senai" title="Senai" height="96" width="175"></div>
          <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-3.jpg" alt="Itaú" title="Itaú" height="96" width="175"></div>
          <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-4.jpg" alt="Rede Globo" title="Rede Globo" height="96" width="175"></div>
          <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-5.jpg" alt="Rede bandeirantes" title="Rede bandeirantes" height="96" width="175"></div>
          <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-6.jpg" alt="OAS" title="OAS" height="96" width="175"></div>
          <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-7.jpg" alt="Banco Votorantim" title="Banco Votorantim" height="96" width="175"></div>
        </div>
        <div class="border-bottom"></div>
      </div>
      <div class="clearfix"></div>
    </div>
    <!-- <div class="form-step">
      <div class="gridD">
        <div class="left"><img src="<?php //echo get_template_directory_uri(); ?>/assets/img/man-left.jpg" alt="" title=""></div>
        <div class="right">
          <div class="title">
            <h2>Seja parceiro</h2>
            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
          </div>
          <div class="form">
            <form>
              <input type="text" placeholder="Seu Nome">
              <input type="text" placeholder="Seu E-mail">
              <input type="text" placeholder="Seu Telefone">
              <textarea placeholder="Sua Mensagem"></textarea>
              <button type="submit"></button>
            </form>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div> -->
    <?php get_template_part( 'inc/parceiro-form' ); ?>
    <?php get_template_part( 'inc/cadastre-form' ); ?>
    <!-- <div style="margin-bottom: -35px;" class="seja-parceiro"><a href="#" alt=""><img src="<?php //echo get_template_directory_uri(); ?>/assets/img/seja-parceiro-mobile.jpg" alt="" title=""></a></div> -->
    <!-- <div class="newsletter-step">
      <div class="gridD">
        <div class="overlay-beige"></div>
        <div class="left">
          <div class="title">
            <h2>CADASTRE E RECEBA NOVIDADES</h2>
            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
          </div>
          <div class="form-news">
            <form>
              <input type="text" placeholder="Seu E-mail">
              <button type="submit"></button>
            </form>
          </div>
        </div>
        <div class="right">
          <div class="title">
            <h2>Tire sua dúvida</h2>
            <p> Diga-nos suas dúvidas e <br>em breve responderemos :)</p>
          </div>
          <div class="faq-form">
            <form>
              <input type="text" placeholder="Seu Nome">
              <input type="text" placeholder="Seu E-mail">
              <textarea placeholder="Sua Dúvida"></textarea>
              <button type="submit"></button>
            </form>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div> -->
    <?php get_footer();?>