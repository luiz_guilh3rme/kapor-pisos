<?php
/**
 * Template Name: Modelo contato
 *
 * 
 *
 * @package WordPress
 * @subpackage Kapor_Pisos
 * @since Kapor Pisos 1.0
 */
get_header();
?>
<div class="ondeestamos-step">
    <div class="gridD">
        <div class="left">
            <div class="topic-header">
                <ul>
                    <li>Assoalhos de madeira</li>
                    <li>Assoalhos de demolição</li>
                </ul>
            </div>
            <div class="content-resume-categoria">
                <div class="title">
                    <h1>Onde estamos</h1>
                </div>
                <div class="content">
                    <p>
                        <strong>Showroom</strong>
                        <br>
                        Rua Alvarenga, nº 1104 – Butantã – São Paulo/SP
                        <br>Tel: (11) 4750-2944
                        <br>Horário de funcionamento (Showroom)
                        <br>
                        Seg a sex das 08 às 19:00 hrs
                        <br>
                        Aos sábados das 8 às 15:00 hrs
                    </p>
                    <p><strong>Deposito: </strong><br>Av. Corifeu de Azevedo Marques, nº 700 – Butantã – São Paulo/SP<br>Tel:
                        (11) 3723-2970<br>
                    Horário de funcionamento
                        <br>
                        Seg a sex das 08 às 19:00 hrs
                    </p>
                    <p><strong>Serraria/Deposito: </strong><br>Rua das Antilhas , 183 – Barueri – SP<br>Email de
                        vendas: vendas@kapor.com.br:<br>
                        contato@kapor.com.br </p>
                
                    <div class="more-details"><a href="#" title="">faça um orçamento</a></div>
                  <div class="badges-contact">
                        <img style="max-width: 45%; margin-top: 30px;" src="<?php bloginfo('template_url') ?>/assets/img/selo-casa-cor-2017.png"
                            alt="Selo Casa Cor 2017">

                        <img style="max-width: 45%; margin-top: 30px;" src="<?php bloginfo('template_url') ?>/assets/img/bonacert.png"
                            alt="Selo Bonacert Certified">

                        <img style="max-width: 45%; margin-top: 30px;" src="<?php bloginfo('template_url') ?>/assets/img/selo-casa-cor-2018.png"
                            alt="Selo Casa Cor 2018">

                        <img style="max-width: 45%; margin-top: 30px;" src="<?php bloginfo('template_url') ?>/assets/img/selo-casa-cor-2019.png"
                            alt="Selo Casa Cor 2019">
                    </div>

                </div>
            </div>
        </div>
        <div class="right">
            <div id="map"></div>
            <div class="cta-form-contact-wrapper">
                <p class="formtitle">
                    FAÇA SEU ORÇAMENTO AGORA!
                </p>
                <?php echo do_shortcode('[contact-form-7 id="13" title="Orçamento"]'); ?>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php get_template_part( 'inc/parceiro-form' ); ?>
<div class="clearfix"></div>
<?php get_footer();?>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC7l5XWCJmYxTVE9CE4GQ4tUXWN1t2klzY&callback=initMap"></script>
<script>
    $(document).ready(function () {
        $('.ondeestamos-step').addClass('roboto');
    });
</script>
<style>
    .roboto a,
    .roboto p:not(.formtitle),
    .roboto {
        font-family: 'Roboto' !important;
    }
</style>