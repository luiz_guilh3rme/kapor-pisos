<?php
$_SESSION['uri'] = $_SERVER['SERVER_NAME'];
/**
 * Template Kapor Pisos
 *
 *
 *
 * @package WordPress
 * @subpackage Kapor_Pisos
 * @since Kapor Pisos 1.0
 */
?>
<!DOCTYPE html>
<html lang="pt-br">

    <head>
        <!-- Google Tag Manager -->
        <script>
            (function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({
                    'gtm.start': new Date().getTime(),
                    event: 'gtm.js'
                });
                var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s),
                    dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-PC8S7QS');
        </script>
        <!-- End Google Tag Manager -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <title><?php echo get_the_title( $ID ); ?> | Kapor Pisos</title>
        <meta content="<?php echo get_post_meta(get_the_ID(), '_yoast_wpseo_metadesc', true);  ?>" name="description">
        <link rel="canonical" href="https://www.kaporpisos.com.br<?php echo $_SERVER['REQUEST_URI'];?>">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/css/lib/font-awesome.css" rel="stylesheet"
            type="text/css">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/css/lib/slick.css" rel="stylesheet"
            type="text/css">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/css/style.css" rel="stylesheet" type="text/css">
        <style type="text/css">
            .details-step .right .orcamento-form form button,
            .details-step .right .orcamento-form form input[type=submit] {
                bottom: -355px !important;
            }
        </style>


        <!-- <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon.png"> -->
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon.ico">
        <!--[if IE 8]>
        <body class="ie ie8"></body><![endif]-->
        <!--[if IE 9]>
        <body class="ie ie9"></body><![endif]-->

        <!--Start of Zendesk Chat Script-->
        <script type="text/javascript">
            window.onload = function () {
                if (window.innerWidth > 800) {
                    window.$zopim || (function (d, s) {
                        var z = $zopim = function (c) {
                                z._.push(c)
                            },
                            $ = z.s =
                            d.createElement(s),
                            e = d.getElementsByTagName(s)[0];
                        z.set = function (o) {
                            z.set.
                            _.push(o)
                        };
                        z._ = [];
                        z.set._ = [];
                        $.async = !0;
                        $.setAttribute("charset", "utf-8");
                        $.src = "https://v2.zopim.com/?5WDFcfaOGR60i7vRjATy07XVf4oNkWQg";
                        z.t = +new Date;
                        $.
                        type = "text/javascript";
                        e.parentNode.insertBefore($, e)
                    })(document, "script");
                }
            }

        </script>
        <!--End of Zendesk Chat Script-->

        <script>
        console.log('foo')
        </script>
    </head>

    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PC8S7QS"
                height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <!-- HEADER-->
        <div id="headerMain">
            <div class="logo"><a href="/" title="#"><img
                        src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="Logo Kapor Pisos "
                        title="Ir para Home " height="47" width="151"></a></div>
            <div class="nav">
                <ul>
                    <li><a href="http://www.kaporpisos.com.br/empresa/" title="Quem Somos">a empresa</a></li>
                    <li class="sub-icon"><a href="#" title="Produtos">produtos</a>
                        <div class="sub-nav">
                            <ul>
                                <li><a href="http://www.kaporpisos.com.br/assoalho-de-demolicao/"
                                        title="Ir para Assoalho de Demolição ">Assoalho de Demolição</a></li>
                                <li><a href="http://www.kaporpisos.com.br/assoalhos-de-madeira-tradicionais/"
                                        title="Ir para Assoalho de Madeira ">Assoalho de Madeira</a></li>
                                <li><a href="http://www.kaporpisos.com.br/brise-de-madeira/"
                                        title="Ir para Brisé de Madeira ">Brisé de Madeira</a></li>
                                <li><a href="https://www.kaporpisos.com.br/carpete-de-nylon"
                                        title="Ir para Carpetes de Nylon">Carpetes de Nylon</a></li>
                                <li><a href="http://www.kaporpisos.com.br/corrimao-de-madeira/"
                                        title="Ir para Corrimão de Madeira ">Corrimão de Madeira</a></li>
                                <li><a href="http://www.kaporpisos.com.br/deck-de-madeira/"
                                        title="Ir para Deck de Madeira ">Deck de Madeira</a></li>
                                <li><a href="http://www.kaporpisos.com.br/escadas-de-madeira/"
                                        title="Ir para Escadas de Madeira ">Escadas de Madeira</a></li>
                                <li><a href="http://www.kaporpisos.com.br/forro-de-madeira/"
                                        title="Ir para Forro de Madeira ">Forro de Madeira</a></li>
                                <li><a href="http://www.kaporpisos.com.br/painel-de-madeira/"
                                        title="Ir para Painel de Madeira ">Painel de Madeira</a></li>
                                <li><a href="https://www.kaporpisos.com.br/pergolados-de-madeira"
                                        title="Ir para Pergolado de Madeira">Pergolado de Madeira</a></li>
                                <li><a href="http://www.kaporpisos.com.br/piso-de-bambu/"
                                        title="Ir para Piso de Bambu ">Piso de Bambu</a></li>
                                <li><a href="http://www.kaporpisos.com.br/piso-de-madeira/"
                                        title="Ir para Piso de Madeira ">Piso de Madeira</a></li>
                                <li><a href="http://www.kaporpisos.com.br/piso-laminado/"
                                        title="Ir para Piso Laminado ">Piso Laminado</a></li>
                                <li><a href="http://www.kaporpisos.com.br/piso-pronto-macico/"
                                        title="Ir para Piso Pronto Maciço ">Piso Pronto Maciço</a></li>
                                <li><a href="http://www.kaporpisos.com.br/piso-pronto-estruturado/"
                                        title="Ir para Piso Pronto Estruturado ">Piso Pronto Estruturado</a></li>
                                <li><a href="http://www.kaporpisos.com.br/piso-vinilico/"
                                        title="Ir para Piso Vinílico ">Piso Vinílico</a></li>
                                <li><a href="https://www.kaporpisos.com.br/rodapes-de-madeira"
                                        title="Ir para Rodapé de Madeira">Rodapé de Madeira</a></li>
                                <li><a href="https://www.kaporpisos.com.br/tacos-de-madeira/"
                                        title="Ir para Taco de Madeira">Taco de Madeira</a></li>
                                <li><a href="http://www.kaporpisos.com.br/vigamento-para-telhado/"
                                        title="Ir para Vigamento para Telhados ">Vigamento para Telhados</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="sub-icon"><a href="#" title="Serviços">Serviços</a>
                        <div class="sub-nav">
                            <ul>
                                <li><a href="http://www.kaporpisos.com.br/aplicacao-de-resina-bona/"
                                        title="Ir para Aplicação de Resina Bona ">Aplicação de Resina Bona</a></li>
                                <li><a href="http://www.kaporpisos.com.br/aplicacao-de-resina-sinteco/"
                                        title="Ir para Aplicação de Resina Sinteco ">Aplicação de Resina Sinteco</a>
                                </li>
                                <li><a href="http://www.kaporpisos.com.br/aplicacao-de-resina-skania/"
                                        title="Ir para Aplicação de Resina Skania ">Aplicação de Resina Skania</a></li>
                                <li><a href="http://www.kaporpisos.com.br/instalacao-e-acabamento-de-piso/"
                                        title="Ir para Instalação e Acabamento de Piso ">Instalação e Acabamento de
                                        Piso</a></li>
                                <li><a href="http://www.kaporpisos.com.br/raspagem-de-taco-e-aplicacao-de-resina/"
                                        title="Ir para Raspagem de Taco e Aplicação de Resina ">Raspagem de Taco e
                                        Aplicação de Resina</a></li>
                                <li><a href="http://www.kaporpisos.com.br/raspagem-e-aplicacao-de-resina-bona/"
                                        title="Ir para Raspagem e Aplicação de Resina Bona ">Raspagem e Aplicação de
                                        Resina Bona</a></li>
                            </ul>
                        </div>
                    </li>
                    <li><a href="#" title="Tipos de Madeira">tipos de madeira</a>
                        <div class="sub-nav">
                            <ul>
                                <li><a href="https://www.kaporpisos.com.br/madeira-carvalho-americano"
                                        title="Ir para Carvalho Americano ">Carvalho Americano</a></li>
                                <li><a href="https://www.kaporpisos.com.br/madeira-carvalho-europeu"
                                        title="Ir para Carvalho Europeu  ">Carvalho Europeu</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-amendoim/"
                                        title="Ir para Madeira Amendoim ">Madeira Amendoim</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-amendoa/"
                                        title="Ir para Madeira Amêndoa ">Madeira Amêndoa</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-angelim/"
                                        title="Ir para Madeira Angelim ">Madeira Angelim</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-canela/"
                                        title="Ir para Madeira Canela ">Madeira Canela</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-cedro-rosa/"
                                        title="Ir para Madeira Cedro Rosa ">Madeira Cedro Rosa</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-cumaru/"
                                        title="Ir para Madeira Cumaru ">Madeira Cumaru</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-grapia/"
                                        title="Ir para Madeira Grápia ">Madeira Grápia </a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-guajuvira"
                                        title="Ir para Madeira Guajuvira ">Madeira Guajuvira </a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-ipe/"
                                        title="Ir para Madeira Ipê ">Madeira Ipê</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-itauba/"
                                        title="Ir para Madeira Itaúba ">Madeira Itaúba</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-jatoba/"
                                        title="Ir para Madeira Jatobá ">Madeira Jatobá</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-macica/"
                                        title="Ir para Madeira Maciça ">Madeira Maciça</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-muiracatiara/"
                                        title="Ir para Madeira Muiracatiara ">Madeira Muiracatiara</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-peroba-clara"
                                        title="Ir para Madeira Peroba Clara">Madeira Peroba Clara</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-peroba-de-demolicao"
                                        title="Ir para Madeira Peroba de Demolição">Madeira Peroba de Demolição</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-peroba-mica/"
                                        title="Ir para Madeira Peroba Mica">Madeira Peroba Mica</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-sucupira/"
                                        title="Ir para Madeira Sucupira">Madeira Sucupira</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-tauari/"
                                        title="Ir para Madeira Tauari  ">Madeira Tauari </a></li>

                            </ul>
                        </div>
                    </li>

                    <li><a href="#" title="Marcas">marcas</a>
                        <div class="sub-nav">
                            <ul>
                                <li><a href="https://www.kaporpisos.com.br/pisos-e-carpetes-beaulieu"
                                        title="Ir para Pisos e Carpetes Beaulieu">Pisos e Carpetes Beaulieu</a></li>
                                <li><a href="https://www.kaporpisos.com.br/pisos-e-rodapes-durafloor"
                                        title="Ir para Pisos e Rodapés Durafloor">Pisos e Rodapés Durafloor</a></li>
                                <li><a href="https://www.kaporpisos.com.br/pisos-e-rodapes-eucafloor"
                                        title="Ir para Pisos e Rodapés Eucafloor">Pisos e Rodapés Eucafloor</a></li>
                                <li><a href="https://www.kaporpisos.com.br/pisos-masterpiso"
                                        title="Ir para Pisos Masterpisos">Pisos Masterpisos</a></li>
                                <li><a href="https://www.kaporpisos.com.br/pisos-quick-step"
                                        title="Ir para Pisos Quickstep">Pisos Quickstep</a></li>
                                <li><a href="https://www.kaporpisos.com.br/rodapes-santa-luzia"
                                        title="Ir para Rodapés Santa Luzia">Rodapés Santa Luzia</a></li>
                                <li><a href="https://www.kaporpisos.com.br/pisos-tarkett"
                                        title="Ir para Pisos Tarkett">Pisos Tarkett</a></li>
                            </ul>
                        </div>
                    </li>
                    <li><a href="http://www.kaporpisos.com.br/parceiros/" title="Parceiros">parceiros</a></li>
                    <li><a href="http://www.kaporpisos.com.br/galeria/" title="Galeria">galeria</a></li>
                    <li><a href="http://www.kaporpisos.com.br/blog/" title="Blog">blog</a></li>
                    <li><a href="http://www.kaporpisos.com.br/contato/" title="Contato">contato</a></li>
                </ul>
            </div>
            <div class="adress-nav">
                <a href="tel:1130932010" title="Ligar para o Showroom da Kapor Pisos" class="tel-lateral">(11)
                    3093-2010</a>
            </div>
            <div class="cta-nav">
                <p>Nós te ligamos</p>
            </div>
            <div class="social-network-wrapper">
                <a href="https://www.facebook.com/pisodemadeirakapor/" target="_BLANK"><i class="fa fa-facebook"></i></a>
                <a href="https://instagram.com/kaporpisos/" target="_BLANK"><i class="fa fa-instagram"></i></a>
            </div>
        </div>
        <div id="headerMainMobile">
            <div class="logo"><a href="/" title="#"><img
                        src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="Logo Kapor Pisos "
                        title="Ir para Home " height="47" width="151"></a></div>
            <div class="nav">
                <ul>
                    <li><a href="http://www.kaporpisos.com.br/empresa/" title="#">a empresa</a></li>
                    <li class="active-sub-mobile"><a href="#" title="#">produtos</a>
                        <div class="sub-nav">
                            <div class="btn-close-subnav"></div>
                            <ul>
                                <li><a href="http://www.kaporpisos.com.br/assoalho-de-demolicao/"
                                        title="Ir para Assoalho de Demolição ">Assoalho de Demolição</a></li>
                                <li><a href="http://www.kaporpisos.com.br/assoalhos-de-madeira-tradicionais/"
                                        title="Ir para Assoalho de Madeira ">Assoalho de Madeira</a></li>
                                <li><a href="http://www.kaporpisos.com.br/brise-de-madeira/"
                                        title="Ir para Brisé de Madeira ">Brisé de Madeira</a></li>
                                <li><a href="#" title="Ir para Carpetes de Nylon">Carpetes de Nylon</a></li>
                                <li><a href="http://www.kaporpisos.com.br/corrimao-de-madeira/"
                                        title="Ir para Corrimão de Madeira ">Corrimão de Madeira</a></li>
                                <li><a href="http://www.kaporpisos.com.br/deck-de-madeira/"
                                        title="Ir para Deck de Madeira ">Deck de Madeira</a></li>
                                <li><a href="http://www.kaporpisos.com.br/escadas-de-madeira/"
                                        title="Ir para Escadas de Madeira ">Escadas de Madeira</a></li>
                                <li><a href="http://www.kaporpisos.com.br/forro-de-madeira/"
                                        title="Ir para Forro de Madeira ">Forro de Madeira</a></li>
                                <li><a href="http://www.kaporpisos.com.br/painel-de-madeira/"
                                        title="Ir para Painel de Madeira ">Painel de Madeira</a></li>
                                <li><a href="#" title="Ir para Pergolado de Madeira">Pergolado de Madeira</a></li>
                                <li><a href="http://www.kaporpisos.com.br/piso-de-bambu/"
                                        title="Ir para Piso de Bambu ">Piso de Bambu</a></li>
                                <li><a href="http://www.kaporpisos.com.br/piso-de-madeira/"
                                        title="Ir para Piso de Madeira ">Piso de Madeira</a></li>
                                <li><a href="http://www.kaporpisos.com.br/piso-laminado/"
                                        title="Ir para Piso Laminado ">Piso Laminado</a></li>
                                <li><a href="http://www.kaporpisos.com.br/piso-pronto-estruturado/"
                                        title="Ir para Piso Pronto Estruturado ">Piso Pronto Estruturado</a></li>
                                <li><a href="http://www.kaporpisos.com.br/piso-vinilico/"
                                        title="Ir para Piso Vinílico ">Piso Vinílico</a></li>
                                <li><a href="#" title="Ir para Rodapé de Madeira">Rodapé de Madeira</a></li>
                                <li><a href="https://www.kaporpisos.com.br/tacos-de-madeira/"
                                        title="Ir para Taco de Madeira">Taco de Madeira</a></li>
                                <li><a href="http://www.kaporpisos.com.br/vigamento-para-telhado/"
                                        title="Ir para Vigamento para Telhados ">Vigamento para Telhados</a></li>
                            </ul>
                        </div>
                    </li>

                    <li class="active-sub-mobile"><a href="#" title="#">serviços</a>
                        <div class="sub-nav">
                            <div class="btn-close-subnav"></div>
                            <ul>
                                <li><a href="http://www.kaporpisos.com.br/aplicacao-de-resina-bona/"
                                        title="Ir para Aplicação de Resina Bona ">Aplicação de Resina Bona</a></li>
                                <li><a href="http://www.kaporpisos.com.br/aplicacao-de-resina-sinteco/"
                                        title="Ir para Aplicação de Resina Sinteco ">Aplicação de Resina Sinteco</a>
                                </li>
                                <li><a href="http://www.kaporpisos.com.br/aplicacao-de-resina-skania/"
                                        title="Ir para Aplicação de Resina Skania ">Aplicação de Resina Skania</a></li>
                                <li><a href="http://www.kaporpisos.com.br/instalacao-e-acabamento-de-piso/"
                                        title="Ir para Instalação e Acabamento de Piso ">Instalação e Acabamento de
                                        Piso</a></li>
                                <li><a href="http://www.kaporpisos.com.br/raspagem-de-taco-e-aplicacao-de-resina/"
                                        title="Ir para Raspagem de Taco e Aplicação de Resina ">Raspagem de Taco e
                                        Aplicação de Resina</a></li>
                                <li><a href="http://www.kaporpisos.com.br/raspagem-e-aplicacao-de-resina-bona/"
                                        title="Ir para Raspagem e Aplicação de Resina Bona ">Raspagem e Aplicação de
                                        Resina Bona</a></li>


                            </ul>
                        </div>
                    </li>

                    <li class="active-sub-mobile"><a href="#" title="#">tipos de madeira</a>
                        <div class="sub-nav">
                            <div class="btn-close-subnav"></div>
                            <ul>
                                <li><a href="#" title="Ir para Carvalho Americano ">Carvalho Americano</a></li>
                                <li><a href="#" title="Ir para Carvalho Europeu  ">Carvalho Europeu</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-amendoim/"
                                        title="Ir para Madeira Amendoim ">Madeira Amendoim</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-amendoa/"
                                        title="Ir para Madeira Amêndoa ">Madeira Amêndoa</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-angelim/"
                                        title="Ir para Madeira Angelim ">Madeira Angelim</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-canela/"
                                        title="Ir para Madeira Canela ">Madeira Canela</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-cedro-rosa/"
                                        title="Ir para Madeira Cedro Rosa ">Madeira Cedro Rosa</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-cumaru/"
                                        title="Ir para Madeira Cumaru ">Madeira Cumaru</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-grapia/"
                                        title="Ir para Madeira Grápia ">Madeira Grápia </a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-guajuvira"
                                        title="Ir para Madeira Guajuvira ">Madeira Guajuvira </a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-ipe/"
                                        title="Ir para Madeira Ipê ">Madeira Ipê</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-itauba/"
                                        title="Ir para Madeira Itaúba ">Madeira Itaúba</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-jatoba/"
                                        title="Ir para Madeira Jatobá ">Madeira Jatobá</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-macica/"
                                        title="Ir para Madeira Maciça ">Madeira Maciça</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-muiracatiara/"
                                        title="Ir para Madeira Muiracatiara ">Madeira Muiracatiara</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-peroba-clara"
                                        title="Ir para Madeira Peroba Clara">Madeira Peroba Clara</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-peroba-de-demolicao"
                                        title="Ir para Madeira Peroba de Demolição">Madeira Peroba de Demolição</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-peroba-mica/"
                                        title="Ir para Madeira Peroba Mica">Madeira Peroba Mica</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-sucupira/"
                                        title="Ir para Madeira Sucupira">Madeira Sucupira</a></li>
                                <li><a href="http://www.kaporpisos.com.br/madeira-tauari/"
                                        title="Ir para Madeira Tauari  ">Madeira Tauari </a></li>

                            </ul>
                        </div>
                    </li>

                    <li class="active-sub-mobile"><a href="#" title="Marcas">marcas</a>
                        <div class="sub-nav">
                            <div class="btn-close-subnav"></div>
                            <ul>
                                <li><a href="#" title="Ir para Pisos e Carpetes Beaulieu">Pisos e Carpetes Beaulieu</a>
                                </li>
                                <li><a href="#" title="Ir para Pisos e Rodapés Durafloor">Pisos e Rodapés Durafloor</a>
                                </li>
                                <li><a href="#" title="Ir para Pisos e Rodapés Eucafloor">Pisos e Rodapés Eucafloor</a>
                                </li>
                                <li><a href="#" title="Ir para Pisos Masterpisos">Pisos Masterpisos</a></li>
                                <li><a href="#" title="Ir para Pisos Quickstep">Pisos Quickstep</a></li>
                                <li><a href="#" title="Ir para Rodapés Santa Luzia">Rodapés Santa Luzia</a></li>
                                <li><a href="#" title="Ir para Pisos Tarkett">Pisos Tarkett</a></li>
                            </ul>
                        </div>
                    <li><a href="http://www.kaporpisos.com.br/parceiros/" title="Parceiros">parceiros</a></li>
                    <li><a href="http://www.kaporpisos.com.br/galeria/" title="Galeria">galeria</a></li>
                    <li><a href="http://www.kaporpisos.com.br/blog/" title="Blog">blog</a></li>
                    <li><a href="http://www.kaporpisos.com.br/contato/" title="Contato">contato</a></li>
                </ul>
            </div>
            <div class="btn-open-nav"></div>
        </div>