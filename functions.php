
<?php
/**
 * Template Kapor Pisos
 *
 * 
 *
 * @package WordPress
 * @subpackage Kapor_Pisos
 * @since Kapor Pisos 1.0
 */


function my_function_admin_bar(){
    return false;
}
add_filter( 'show_admin_bar' , 'my_function_admin_bar');


add_theme_support( 'post-thumbnails' );

//DESTAQUE-PRODUTOS
add_image_size( 'produtos-destaque', 1035, 1080, true );
add_image_size( 'produtos-thumb', 555, 270, true );
//BLOG
add_image_size( 'blog-home', 580, 300, true );
add_image_size( 'blog-full', 700, 300, true );
add_image_size( 'blog-single', 1035, 1080, true );

//SERVICOS-DESTAQUE
add_image_size( 'servicos-destaque', 1280, 705, true );
add_image_size( 'servicos-thumb', 263, 99999, true );

add_image_size( 'depo-thumb', 900, 600, true );
//HOME

// POST TYPE PRODUTOS
add_action('init', 'type_post_produtos');

function type_post_produtos() { 
	$labels = array(
		'name' => _x('Produtos', 'post type general name'),
		'singular_name' => _x('Produto', 'post type singular name'),
		'add_new' => _x('Adicionar Novo', 'Novo item'),
		'add_new_item' => __('Novo Item'),
		'edit_item' => __('Editar Item'),
		'new_item' => __('Novo Item'),
		'view_item' => __('Ver Item'),
		'search_items' => __('Procurar Itens'),
		'not_found' =>  __('Nenhum registro encontrado'),
		'not_found_in_trash' => __('Nenhum registro encontrado na lixeira'),
		'parent_item_colon' => '',
		'menu_name' => 'Produtos'
		);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'public_queryable' => true,
		'show_ui' => true,           
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => null,    
		'supports' => array('title','editor','thumbnail', 'excerpt')
		);

	register_post_type( 'produtos' , $args );
	flush_rewrite_rules();
}
register_taxonomy(
	"categorias", 
	"produtos", 
	array(            
		"label" => "Categorias", 
		"singular_label" => "Categoria", 
		"rewrite" => true,
		"hierarchical" => true
		)
	);


// POST TYPE SERVICOS
add_action('init', 'type_post_servicos');

function type_post_servicos() { 
	$labels = array(
		'name' => _x('Serviços', 'post type general name'),
		'singular_name' => _x('Serviço', 'post type singular name'),
		'add_new' => _x('Adicionar Novo', 'Novo item'),
		'add_new_item' => __('Novo Item'),
		'edit_item' => __('Editar Item'),
		'new_item' => __('Novo Item'),
		'view_item' => __('Ver Item'),
		'search_items' => __('Procurar Itens'),
		'not_found' =>  __('Nenhum registro encontrado'),
		'not_found_in_trash' => __('Nenhum registro encontrado na lixeira'),
		'parent_item_colon' => '',
		'menu_name' => 'Serviços'
		);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'public_queryable' => true,
		'show_ui' => true,           
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => null,     
		'supports' => array('title','editor','thumbnail', 'excerpt')
		);

	register_post_type( 'servicos' , $args );
	flush_rewrite_rules();
}
register_taxonomy(
	"categorias", 
	"servicos", 
	array(            
		"label" => "Categorias", 
		"singular_label" => "Categoria", 
		"rewrite" => true,
		"hierarchical" => true
		)
	);

// POST TYPE TIPOS DE MADEIRA
add_action('init', 'type_post_madeiras');

function type_post_madeiras() { 
	$labels = array(
		'name' => _x('Tipos de madeira', 'post type general name'),
		'singular_name' => _x('Tipos de madeira', 'post type singular name'),
		'add_new' => _x('Adicionar Novo', 'Novo item'),
		'add_new_item' => __('Novo Item'),
		'edit_item' => __('Editar Item'),
		'new_item' => __('Novo Item'),
		'view_item' => __('Ver Item'),
		'search_items' => __('Procurar Itens'),
		'not_found' =>  __('Nenhum registro encontrado'),
		'not_found_in_trash' => __('Nenhum registro encontrado na lixeira'),
		'parent_item_colon' => '',
		'menu_name' => 'Tipos de madeira'
		);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'public_queryable' => true,
		'show_ui' => true,           
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => null,     
		'supports' => array('title','editor','thumbnail', 'excerpt')
		);

	register_post_type( 'madeiras' , $args );
	flush_rewrite_rules();
}
register_taxonomy(
	"categorias", 
	"madeiras", 
	array(            
		"label" => "Categorias", 
		"singular_label" => "Categoria", 
		"rewrite" => true,
		"hierarchical" => true
		)
	);

// POST TYPE DEPOIMENTOS
add_action('init', 'type_post_depoimento');

function type_post_depoimento() { 
	$labels = array(
		'name' => _x('Depoimentos', 'post type general name'),
		'singular_name' => _x('Depoimento', 'post type singular name'),
		'add_new' => _x('Adicionar Novo', 'Novo item'),
		'add_new_item' => __('Novo Item'),
		'edit_item' => __('Editar Item'),
		'new_item' => __('Novo Item'),
		'view_item' => __('Ver Item'),
		'search_items' => __('Procurar Itens'),
		'not_found' =>  __('Nenhum registro encontrado'),
		'not_found_in_trash' => __('Nenhum registro encontrado na lixeira'),
		'parent_item_colon' => '',
		'menu_name' => 'Depoimentos'
		);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'public_queryable' => true,
		'show_ui' => true,           
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => null,     
		'supports' => array('title','editor','thumbnail', 'excerpt')
		);

	register_post_type( 'depoimento' , $args );
	flush_rewrite_rules();
}




// METABOX SERVIÇOS
add_filter( 'rwmb_meta_boxes', 'prefix_meta_boxes' );
function prefix_meta_boxes( $meta_boxes ) {
    $meta_boxes[] = array(
        'title'  => 'Aplicações',
        'post_types' => array(
        	'servicos',
        	'madeiras'
        	),
        'fields' => array(
            array(
                'id'   => 'aplica_1',
                'name' => 'Descrição',
                'type' => 'wysiwyg',
            ),
            array(
                'id'   => 'img_madeira',
                'name' => 'Imagem Madeira',
                'type' => 'image_advanced',
            ),
             array(
                'id'   => 'veja-mais',
                'name' => __( 'Link Veja Mais', 'textdomain' ),
                'type' => 'text',
            ),
        ),
    );
    return $meta_boxes;
}


if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_5d8141d096490',
		'title' => 'Tipos de madeira',
		'fields' => array(
			array(
				'key' => 'field_5d81420b0a4e7',
				'label' => 'Madeiras',
				'name' => 'woods',
				'type' => 'repeater',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'collapsed' => '',
				'min' => 0,
				'max' => 0,
				'layout' => 'block',
				'button_label' => '',
				'sub_fields' => array(
					array(
						'key' => 'field_5d8142130a4e8',
						'label' => 'Título',
						'name' => 'title_wood',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_5d81421d0a4e9',
						'label' => 'Imagem',
						'name' => 'image_wood',
						'type' => 'image',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'url',
						'preview_size' => 'medium',
						'library' => 'all',
						'min_width' => '',
						'min_height' => '',
						'min_size' => '',
						'max_width' => '',
						'max_height' => '',
						'max_size' => '',
						'mime_types' => '',
					),
				),
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'produtos',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));
	
	endif;

	add_filter('acf/settings/show_admin', '__return_false');


// REMOVE CUSTOM POST SLUG
add_filter(
	'post_type_link',
	'custom_post_type_link',
	10,
	3
	);

function custom_post_type_link($permalink, $post, $leavename) {
	if (!gettype($post) == 'post') {
		return $permalink;
	}
	switch ($post->post_type) {
		case 'produtos':
		$permalink = get_home_url() . '/' . $post->post_name . '/';
		break;
	}
	switch ($post->post_type) {
		case 'servicos':
		$permalink = get_home_url() . '/' . $post->post_name . '/';
		break;
	}
	switch ($post->post_type) {
		case 'madeiras':
		$permalink = get_home_url() . '/' . $post->post_name . '/';
		break;
	}
	return $permalink;
}

add_action(
	'pre_get_posts',
	'custom_pre_get_posts'
	);

function custom_pre_get_posts($query) {
	global $wpdb;

	if(!$query->is_main_query()) {
		return;
	}

	$post_name = $query->get('name');

	$post_type = $wpdb->get_var(
		$wpdb->prepare(
			'SELECT post_type FROM ' . $wpdb->posts . ' WHERE post_name = %s LIMIT 1',
			$post_name
			)
		);

	switch($post_type) {
		case 'produtos':
		$query->set('produtos', $post_name);
		$query->set('post_type', $post_type);
		$query->is_single = true;
		$query->is_page = false;
		break;
	}
	switch($post_type) {
		case 'servicos':
		$query->set('servicos', $post_name);
		$query->set('post_type', $post_type);
		$query->is_single = true;
		$query->is_page = false;
		break;
	}
	switch($post_type) {
		case 'madeiras':
		$query->set('madeiras', $post_name);
		$query->set('post_type', $post_type);
		$query->is_single = true;
		$query->is_page = false;
		break;
	}
	return $query;
}


// remove junk from head
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);


add_filter( 'pre_site_transient_update_core', create_function( '$a', "return null;" ) );
?>