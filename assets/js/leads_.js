$('form').on('submit', function (e) {
    e.preventDefault();
    var $form = $(this);
    $form.off('submit');
    var dataArray = $form.serializeArray();
    console.log(dataArray);
    
    $.ajax({
        url: '/lead.php',
        type: 'post',
        data: dataArray,
        success: function (data) {
            $form.submit();
            return true;
        },
        error: function (data) {
            console.error('Erro ao enviar lead...');
        }
    });
});