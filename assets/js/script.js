  // ## ## ## ## ## ## ## ## ## ## ##  //
 // ## ## ## ##   JQUERY  ## ## ## ## //
// ## ## ## ## ## ## ## ## ## ## ##  //
$('.todos-depoimentos').slick({
  autoplay: true,
  autoplaySpeed: 4000,
  dots: false,
  arrows: false,
  fade: true,
  cssEase: 'linear'
});


$('.banner-full').slick({
  infinite: true,
  speed: 1500,
  cssEase: 'ease',
  autoplay: true,
  autoplaySpeed: 4000,
  dots: false,
  arrows: true,
});
/*
$(".fechar-whats").click(function(){
    $(".whatsapp").addClass("closed");
});  
*/


$(".modal-mobile .fechar").click(function(e){
    e.preventDefault();
    $(".modal-mobile").removeClass("open");
}); 

$('.banner-categoria-produtos').slick({
  infinite: true,
  speed: 1500,
  cssEase: 'ease',
  autoplay: true,
  autoplaySpeed: 4000,
  dots: false,
  arrows: false,
});
$('.slide-clients-mobile').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  dots: false,
  arrows: true,
  centerMode: true,
  centerPadding: '80px',
});
$('.products-mobile').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  dots: false,
  arrows: true,
  centerMode: true,
  centerPadding: '25px',
});
$('.post-slide-home').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  dots: false,
  arrows: true,
  centerMode: true,
  centerPadding: '25px',
});
$(".sub-icon").hover(
  function () {
    $('body').addClass("space-dots");
  },
  function () {
    $('body').removeClass("space-dots");
  }
  );
function initMap() {
  var myLatLng = {lat: -23.5721433, lng: -46.7111587};

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 14,
    center: myLatLng
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: 'Hello World!'
  });
}
$(".more-details a").click(function(e) {
    e.preventDefault();
    $('body').addClass("opened-modal");
  });
$(".bt-more a").click(function(e) {
    e.preventDefault();
    $('body').addClass("opened-modal");
  });
$(".close-modal").click(function(e) {
    e.preventDefault();
    $('body').removeClass("opened-modal");
  });


$(".cta-nav").click(function(e) {
    e.preventDefault();
    $('body').addClass("opened-cta-nav");
  });
$(".close-modal").click(function(e) {
    e.preventDefault();
    $('body').removeClass("opened-cta-nav");
  });


$( ".btn-open-nav" ).click(function() {
  $( "body" ).toggleClass("opened-nav-mobile");
});

$( ".active-sub-mobile, .btn-close-subnav" ).click(function() {
  $( this ).toggleClass("opened-nav-mobile-sub");
});

$( ".cta-mobile .left" ).click(function() {
  $( "body" ).toggleClass("opened-cta-mobile");
});

