<?php
/**
 * Template Kapor Pisos
 *
 *
 *
 * @package WordPress
 * @subpackage Kapor_Pisos
 * @since Kapor Pisos 1.0
 */
get_header();
?>
<style type="text/css">
  .banner-full .slide1.img1 {
    background-image: url(../img/kapor-pisos-slider.jpg);
  }

  .banner-full .slide2 .img2 {
    background-image: url(../img/kapor-pisos-slider-2.jpg);
  }

  .banner-full .slide2 .img3 {
    background-image: url(../img/kapor-pisos-slider-3.jpg);
  }
</style>
<div class="banner-full">
  <div class="slide">
    <div class="img"></div>
  </div>
  <div class="slide">
    <div class="img"></div>
  </div>

</div>
<div class="about-us-home">
  <div class="gridD">
    <div class="left">
      <div class="title">
        <h1>Sobre a Kapor Pisos</h1>
      </div>
      <div class="content">
        <p>Desde sua fundação, em 1945, a Kapor tem trabalhado, crescido e se tornado referência no ramo de
          revestimentos em madeira. Em décadas de tradição, a Kapor já realizou o sonho de muitas pessoas de obter pisos
          e demais produtos em madeira de alta qualidade, auxiliando na composição de ambientes bonitos, modernos e
          confortáveis. Nossos produtos e serviços são executados por experientes e capacitados profissionais.
          Responsabilidade, eficiência e profissionalismo são os valores que norteiam nosso trabalho.</p><a
          href="/empresa" title="Ir para Sobre Nós">mais</a>
      </div>
    </div>
    <div class="right"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/about-us-photo.jpg" alt=""
        title="" height="705" width="1095"></div>
  </div>
  <div class="clearfix"></div>
</div>
<div class="oque-fazemos">
  <div class="gridD">
    <div class="left">
      <div class="thumb-oque"><a href="#" title=""><img
            src="<?php echo get_template_directory_uri(); ?>/assets/img/thumb-que-fazemos.jpg" alt="" title=""
            height="auto" width="227">
          <h2>Raspagem de Taco</h2>
        </a></div>
      <div class="thumb-oque"><a href="#" title=""><img
            src="<?php echo get_template_directory_uri(); ?>/assets/img/thumb-que-fazemos.jpg" alt="" title=""
            height="auto" width="227">
          <h2>Raspagem e aplicação de resina Bona</h2>
        </a></div>
      <div class="thumb-oque"><a href="#" title=""><img
            src="<?php echo get_template_directory_uri(); ?>/assets/img/thumb-que-fazemos.jpg" alt="" title=""
            height="auto" width="227">
          <h2>INSTALAÇÃO E ACABAMENTO</h2>
        </a></div>
      <div class="thumb-oque"><a href="#" title=""><img
            src="<?php echo get_template_directory_uri(); ?>/assets/img/thumb-que-fazemos.jpg" alt="" title=""
            height="auto" width="227">
          <h2>APLICAÇÃO DE BONA</h2>
        </a></div>
      <div class="thumb-oque"><a href="#" title=""><img
            src="<?php echo get_template_directory_uri(); ?>/assets/img/thumb-que-fazemos.jpg" alt="" title=""
            height="auto" width="227">
          <h2>APLICAÇÃO DE SINTEKO</h2>
        </a></div>
      <div class="thumb-oque"><a href="#" title=""><img
            src="<?php echo get_template_directory_uri(); ?>/assets/img/thumb-que-fazemos.jpg" alt="" title=""
            height="auto" width="227">
          <h2>APLICAÇÃO SKANIA</h2>
        </a></div>
      <div class="more-details"><a href="#" title="">faça um orçamento</a></div>
    </div>
    <div class="right">
      <div class="title">
        <h2>O QUE FAZEMOS?</h2>
      </div>
      <div class="content">
        <p>Trabalhamos com os mais diversos tipos de produtos em madeira. Além de diferenciados tipos de piso (assoalho,
          tacos, pisos laminados, pisos vinílicos e pisos estruturados), também oferecemos brisés, painéis, pergolados,
          decks, escadas, forros, rodapés, vigamentos e revestimentos diversos. Em cada projeto, utilizamos
          exclusivamente as mais nobres espécies de madeira, caracterizadas por sua resistência e pela atratividade
          estética incomparável, levando a beleza rústica da madeira aos mais diversos ambientes. Além disso, nossos
          processos são marcados pela sustentabilidade, com respeito ao meio ambiente. Nossa madeireira oferece
          materiais certificados e emprega as tecnologias mais avançadas em rigorosas linhas de produção.
          <p>
            Oferecemos também os serviços de aplicação de resinas (os melhores produtos do mercado), raspagem,
            instalação e acabamentos de tacos e demais pisos. Contamos com equipes de profissionais experientes e
            qualificados, executando cada projeto com agilidade, praticidade e dedicação. Estamos prontos para orientar
            nossos clientes em cada projeto e realizar sonhos.</p><a href="#" title=""><img
              src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-right.png" alt="" title="" height="14"
              width="59"></a>
      </div>
      <div class="overlay-blue"></div>
    </div>
    <div class="left-mobile">
      <div class="title">
        <h2>O QUE FAZEMOS?</h2>
      </div>
      <div class="thumb-oque"><a href="#" title=""><img
            src="<?php echo get_template_directory_uri(); ?>/assets/img/thumb-que-fazemos.jpg" alt="" title=""
            height="auto" width="227">
          <h2>Raspagem de Taco</h2>
        </a></div>
      <div class="thumb-oque"><a href="#" title=""><img
            src="<?php echo get_template_directory_uri(); ?>/assets/img/thumb-que-fazemos.jpg" alt="" title=""
            height="auto" width="227">
          <h2>Raspagem e aplicação de resina Bona</h2>
        </a></div>
      <div class="thumb-oque"><a href="#" title=""><img
            src="<?php echo get_template_directory_uri(); ?>/assets/img/thumb-que-fazemos.jpg" alt="" title=""
            height="auto" width="227">
          <h2>INSTALAÇÃO E ACABAMENTO</h2>
        </a></div>
      <div class="thumb-oque"><a href="#" title=""><img
            src="<?php echo get_template_directory_uri(); ?>/assets/img/thumb-que-fazemos.jpg" alt="" title=""
            height="auto" width="227">
          <h2>APLICAÇÃO DE BONA</h2>
        </a></div>
      <div class="thumb-oque"><a href="#" title=""><img
            src="<?php echo get_template_directory_uri(); ?>/assets/img/thumb-que-fazemos.jpg" alt="" title=""
            height="auto" width="227">
          <h2>APLICAÇÃO DE SINTEKO</h2>
        </a></div>
      <div class="thumb-oque"><a href="#" title=""><img
            src="<?php echo get_template_directory_uri(); ?>/assets/img/thumb-que-fazemos.jpg" alt="" title=""
            height="auto" width="227">
          <h2>APLICAÇÃO SKANIA</h2>
        </a></div>
      <div class="more-details"><a href="#" title="">faça um orçamento</a></div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>
<div class="clients">
  <div class="gridD">
    <div class="title">
      <h2>Clientes</h2>
    </div>
    <div class="border-top"></div>
    <div class="slide-clients">
      <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client.jpg" alt="" title=""
          height="96" width="175"></div>
      <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-2.jpg" alt="" title=""
          height="96" width="175"></div>
      <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-3.jpg" alt="" title=""
          height="96" width="175"></div>
      <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-4.jpg" alt="" title=""
          height="96" width="175"></div>
      <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-5.jpg" alt="" title=""
          height="96" width="175"></div>
      <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-6.jpg" alt="" title=""
          height="96" width="175"></div>
      <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-7.jpg" alt="" title=""
          height="96" width="175"></div>
    </div>
    <div class="border-bottom"></div>
  </div>
  <div class="clearfix"></div>
</div>
<div class="clients-mobile">
  <div class="gridD">
    <div class="title">
      <h2>Clientes</h2>
    </div>
    <div class="border-top"></div>
    <div class="slide-clients-mobile">
      <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client.jpg" alt="" title=""
          height="96" width="175"></div>
      <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-2.jpg" alt="" title=""
          height="96" width="175"></div>
      <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-3.jpg" alt="" title=""
          height="96" width="175"></div>
      <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-4.jpg" alt="" title=""
          height="96" width="175"></div>
      <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-5.jpg" alt="" title=""
          height="96" width="175"></div>
      <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-6.jpg" alt="" title=""
          height="96" width="175"></div>
      <div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/client-7.jpg" alt="" title=""
          height="96" width="175"></div>
    </div>
    <div class="border-bottom"></div>
  </div>
  <div class="clearfix"></div>
</div>
<div class="products-home">
  <div class="gridD">
    <div class="title-products">
      <div class="title">
        <h2>Produtos em destaque</h2>
        <p>Confira alguns de nossos principais produtos. A durabilidade e a beleza única da madeira levam sofisticação e
          elegância aos ambientes.</p>
      </div>
      <div class="bt-more"><a href="#" title="">faça um orçamento</a></div>
    </div>
    <div class="products">
      <div class="product"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-home-1.jpg" alt=""
          title="">
        <div class="title-static">
          <h2>Assoalho Tradicionais</h2>
        </div>
        <div class="overlay-product"><a href="#" title="">
            <h2>Assoalho Tradicionais</h2>
          </a><a href="#" title="">
            <p>O assoalho tradicional é um produto altamente resistente e durável, deixando o revestimento do seu piso,
              com um ar mais nobre e sofisticado.</p>
          </a><a href="#" title=""><img
              src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-right-white.png" alt="" title=""></a>
        </div>
      </div>
      <div class="product"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-home-2.jpg" alt=""
          title="">
        <div class="title-static">
          <h2>Deck de madeira</h2>
        </div>
        <div class="overlay-product"><a href="#" title="">
            <h2>Deck de madeira</h2>
          </a><a href="#" title="">
            <p>O assoalho tradicional é um produto altamente resistente e durável, deixando o revestimento do seu piso,
              com um ar mais nobre e sofisticado.</p>
          </a><a href="#" title=""><img
              src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-right-white.png" alt="" title=""></a>
        </div>
      </div>
      <div class="product"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-home-3.jpg" alt=""
          title="">
        <div class="title-static">
          <h2>piso pronto</h2>
        </div>
        <div class="overlay-product"><a href="#" title="">
            <h2>piso pronto</h2>
          </a><a href="#" title="">
            <p>O assoalho tradicional é um produto altamente resistente e durável, deixando o revestimento do seu piso,
              com um ar mais nobre e sofisticado.</p>
          </a><a href="#" title=""><img
              src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-right-white.png" alt="" title=""></a>
        </div>
      </div>
      <div class="product"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-home-4.jpg" alt=""
          title="">
        <div class="title-static">
          <h2>vigamento para telhados</h2>
        </div>
        <div class="overlay-product"><a href="#" title="">
            <h2>vigamento para telhados</h2>
          </a><a href="#" title="">
            <p>O assoalho tradicional é um produto altamente resistente e durável, deixando o revestimento do seu piso,
              com um ar mais nobre e sofisticado.</p>
          </a><a href="#" title=""><img
              src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-right-white.png" alt="" title=""></a>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>
<div class="products-home-mobile">
  <div class="gridD">
    <div class="title-products">
      <div class="title">
        <h2>Produtos em destaque</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
          dolore magna aliqua. </p>
      </div>
      <div class="bt-more"><a href="#" title="">faça um orçamento</a></div>
    </div>
    <div class="products-mobile">
      <div class="product-mobile"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-home-1.jpg"
          alt="" title="">
        <div class="title-static">
          <h2>Assoalho Tradicionais</h2>
        </div>
        <div class="overlay-product">
          <h2>Assoalho Tradicionais</h2>
          <p>O assoalho tradicional é um produto altamente resistente e durável, deixando o revestimento do seu piso,
            com um ar mais nobre e sofisticado.</p><a href="#" title=""><img
              src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-right-white.png" alt="" title=""></a>
        </div>
      </div>
      <div class="product-mobile"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-home-2.jpg"
          alt="" title="">
        <div class="title-static">
          <h2>Deck de madeira</h2>
        </div>
        <div class="overlay-product">
          <h2>Deck de madeira</h2>
          <p>O assoalho tradicional é um produto altamente resistente e durável, deixando o revestimento do seu piso,
            com um ar mais nobre e sofisticado.</p><a href="#" title=""><img
              src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-right-white.png" alt="" title=""></a>
        </div>
      </div>
      <div class="product-mobile"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-home-3.jpg"
          alt="" title="">
        <div class="title-static">
          <h2>piso pronto</h2>
        </div>
        <div class="overlay-product">
          <h2>piso pronto</h2>
          <p>O assoalho tradicional é um produto altamente resistente e durável, deixando o revestimento do seu piso,
            com um ar mais nobre e sofisticado.</p><a href="#" title=""><img
              src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-right-white.png" alt="" title=""></a>
        </div>
      </div>
      <div class="product-mobile"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-home-4.jpg"
          alt="" title="">
        <div class="title-static">
          <h2>vigamento para telhados</h2>
        </div>
        <div class="overlay-product">
          <h2>vigamento para telhados</h2>
          <p>O assoalho tradicional é um produto altamente resistente e durável, deixando o revestimento do seu piso,
            com um ar mais nobre e sofisticado.</p><a href="#" title=""><img
              src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-right-white.png" alt="" title=""></a>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>
<div class="depo-sec">
  <div class="gridD">
    <div class="img-left">
      <div class="overlay-beige"></div><img src="<?php echo get_template_directory_uri(); ?>/assets/img/depo.png"
        alt="Nossos Depoimentos" title="Nossos Depoimentos" height="auto" width="900">
    </div>
    <div class="desc-right">
      <div class="title">

      </div>
      <div class="content">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
          dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
          commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
          nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim
          id est laborum. Sed ut perspiciatis unde omnis iste natus</p>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>
<div class="seja-parceiro"><a href="#" alt=""><img
      src="<?php echo get_template_directory_uri(); ?>/assets/img/seja-parceiro-mobile.jpg" alt="Seja Parceiro"
      title="Seja Parceiro" height="auto" width="300"></a></div>
<!-- <div class="form-step">
      <div class="gridD">
        <div class="left"><img src="<?php //echo get_template_directory_uri(); ?>/assets/img/man-left.png" alt="" title=""></div>
        <div class="right">
          <div class="title">
            <h2>Seja parceiro</h2>
            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
          </div>
          <div class="form">
            <form>
              <input type="text" placeholder="Seu Nome">
              <input type="text" placeholder="Seu E-mail">
              <input type="text" placeholder="Seu Telefone">
              <textarea placeholder="Sua Mensagem"></textarea>
              <button type="submit"></button>
            </form>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div> -->
<?php get_template_part( 'inc/parceiro-form' ); ?>
<?php get_template_part( 'inc/cadastre-form' ); ?>
<!-- <div class="newsletter-step">
      <div class="gridD">
        <div class="overlay-beige"></div>
        <div class="left">
          <div class="title">
            <h2>CADASTRE E RECEBA NOVIDADES</h2>
            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
          </div>
          <div class="form-news">
            <form>
              <input type="text" placeholder="Seu E-mail">
              <button type="submit"></button>
            </form>
          </div>
        </div>
        <div class="right">
          <div class="title">
            <h2>Tire sua dúvida</h2>
            <p> Diga-nos suas dúvidas e <br>em breve responderemos :)</p>
          </div>
          <div class="faq-form">
            <form>
              <input type="text" placeholder="Seu Nome">
              <input type="text" placeholder="Seu E-mail">
              <textarea placeholder="Sua Dúvida"></textarea>
              <button type="submit"></button>
            </form>
          </div>
        </div>
      </div>
    </div> -->
<div class="blog-post">
  <div class="gridD">
    <div class="center">
      <div class="title">
        <h2>blog kapor</h2>
        <p>Fique por dentro das principais novidades do ramo de produtos em madeira, além de dicas de manutenção e
          decoração.</p>
      </div>
    </div>
    <div class="post"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/blog-1.jpg" alt="" title="">
      <div class="title-static">
        <h2>Deck de madeira</h2>
        <h2>Deck de madeira</h2>
      </div>
      <div class="overlay-product"><a href="#" title="">
          <h2>Deck de madeira</h2>
        </a><a href="#" title="">
          <p>O assoalho tradicional é um produto altamente resistente e durável, deixando o revestimento do seu piso,
            com um ar mais nobre e sofisticado.</p>
        </a><a href="#" title=""><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-right-white.png"
            alt="" title=""></a></div>
    </div>
    <div class="post"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/blog-2.jpg" alt="" title="">
      <div class="title-static">
        <h2>Deck de madeira</h2>
      </div>
      <div class="overlay-product"><a href="#" title="">
          <h2>Deck de madeira</h2>
        </a><a href="#" title="">
          <p>O assoalho tradicional é um produto altamente resistente e durável, deixando o revestimento do seu piso,
            com um ar mais nobre e sofisticado.</p>
        </a><a href="#" title=""><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-right-white.png"
            alt="" title=""></a></div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>
<div class="blog-post-mobile">
  <div class="gridD">
    <div class="center">
      <div class="title">
        <h2>blog kapor</h2>
        <p>Fique por dentro das principais novidades do ramo de produtos em madeira, além de dicas de manutenção e
          decoração.
        </p>
      </div>
    </div>
    <div class="post-slide-home">
      <div class="post-mobile"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/blog-1.jpg" alt=""
          title="">
        <div class="title-static">
          <h2>Deck de madeira</h2>
        </div>
        <div class="overlay-product">
          <h2>Deck de madeira</h2>
          <p>O assoalho tradicional é um produto altamente resistente e durável, deixando o revestimento do seu piso,
            com um ar mais nobre e sofisticado.</p><a href="#" title=""><img
              src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-right-white.png" alt="" title=""></a>
        </div>
      </div>
      <div class="post-mobile"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/blog-2.jpg" alt=""
          title="">
        <div class="title-static">
          <h2>Deck de madeira</h2>
        </div>
        <div class="overlay-product">
          <h2>Deck de madeira</h2>
          <p>O assoalho tradicional é um produto altamente resistente e durável, deixando o revestimento do seu piso,
            com um ar mais nobre e sofisticado.</p><a href="#" title=""><img
              src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-right-white.png" alt="" title=""></a>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>
<?php get_footer();?>