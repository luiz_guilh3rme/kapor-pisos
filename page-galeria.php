<?php
/**
 * Template Name: Modelo Galeria
 *
 * 
 *
 * @package WordPress
 * @subpackage Kapor_Pisos
 * @since Kapor Pisos 1.0
 */
get_header();

?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div>
      <div class="gridD">
      <?php the_content(); ?>
      </div>
      <div class="clearfix"></div>
      <?php endwhile; endif; ?>
     <!-- <div class="form-step">
      <div class="gridD">
        <div class="left"><img src="<?php //echo get_template_directory_uri(); ?>/assets/img/man-left.jpg" alt="" title=""></div>
        <div class="right">
          <div class="title">
            <h2>Seja parceiro</h2>
            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
          </div>
          <div class="form">
            <form>
              <input type="text" placeholder="Seu Nome">
              <input type="text" placeholder="Seu E-mail">
              <input type="text" placeholder="Seu Telefone">
              <textarea placeholder="Sua Mensagem"></textarea>
              <button type="submit"></button>
            </form>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div> -->
    <?php get_template_part( 'inc/parceiro-form' ); ?>
    <?php get_template_part( 'inc/cadastre-form' ); ?>
    <!-- <div class="newsletter-step">
      <div class="gridD">
        <div class="overlay-beige"></div>
        <div class="left">
          <div class="title">
            <h2>CADASTRE E RECEBA NOVIDADES</h2>
            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
          </div>
          <div class="form-news">
            <form>
              <input type="text" placeholder="Seu E-mail">
              <button type="submit"></button>
            </form>
          </div>
        </div>
        <div class="right">
          <div class="title">
            <h2>Tire sua dúvida</h2>
            <p> Diga-nos suas dúvidas e <br>em breve responderemos :)</p>
          </div>
          <div class="faq-form">
            <form>
              <input type="text" placeholder="Seu Nome">
              <input type="text" placeholder="Seu E-mail">
              <textarea placeholder="Sua Dúvida"></textarea>
              <button type="submit"></button>
            </form>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div> -->

<?php get_footer();?>